usescript = 1;

load kdatalibimg_16
% wfs/bw: 9.8/44
dim = 96; % image dimension

% find next higher power of 2; zero-pad the image to that size
dim2 = 96;%2^ceil(log2(dim));

imglib = reshape(L,[dim dim 3]);

tmp = zeros(dim2,dim2,3);
tmp((dim2-dim)/2+1:(dim2-dim)/2+dim,...
    (dim2-dim)/2+1:(dim2-dim)/2+dim,:) = imglib;
imglib = tmp;

L = col(imglib(:,:,1));

tmp = zeros(dim2,dim2,size(img,3),3);
tmp((dim2-dim)/2+1:(dim2-dim)/2+dim,...
    (dim2-dim)/2+1:(dim2-dim)/2+dim,:,:) = img;

img = tmp;

dim = dim2;

te = 0.016; % seconds, TE
tro = 0.0225; % readout duration, seconds
ti = -tro/2:tro/dim:tro/2-tro/dim;
tte = ti(:)/te+1;
% threshold for 'significant' temperature rises
maskthresh = -0.1;

step = 0.0001;
verbose = 1;

x = (-dim/2:dim/2-1)/dim;
stopthresh = 0.999;


for ind = 19;%2:120
    %ind = 15;
    imghot = squeeze(img(:,:,ind,1));
    
    % fast cs recon
    niters = 25;
    order = 0;
    lam = 10^-2;
    [mhybr,Ahybr,chybr,fhybr] = thermo_hybrid_nounwrap(conj(imghot),order,lam,niters,conj(L));
    mhybr = -1*mhybr;
    fhybr = conj(fhybr);
    %fhybr = abs(imghot).*exp(-1i*angle(fhybr));
    chybr = -1*chybr;
    mhybrsv = mhybr;
    
%     lam = 10^-2;
%     [mhybrl1,Ahybr,chybrl1,fhybr] = thermo_hybrid_l1(conj(imghot),0,lam,conj(L),-maskthresh);
%     mhybrl1 = -1*mhybrl1;
%     fhybr = conj(fhybr);
%     %fhybr = abs(imghot).*exp(-1i*angle(fhybr));
%     chybrl1 = -1*chybrl1;
% 
%     lam = 10^-2;
%     tic
%     mhybrmex = thermo_hybrid_rth(conj(imghot)/1.45,conj(imglib(:,:,1))/1.45,0,lam,-maskthresh,0.999);
%     toc
%     mhybrmex = -1*mhybrmex;

    tic
    if usescript
        mhybr = thermocscorr_mex(real(mhybr),step,complex(imghot),...
            stopthresh,complex(fhybr),chybr,maskthresh,ones(dim2),tro,te,verbose);
    else
        mask = mhybr < maskthresh; % radians
        cols = find(sum(mask,1)); % columns to process
        for ii = cols
            
            imgc = fhybr(:,ii)*exp(1i*chybr);
            
            data1dft = imghot(:,ii) - ~mask(:,ii).*imgc.*exp(1i*mhybr(:,ii));
            data1dft = ft(data1dft);
            
            % iteratively update the map inside the hot spot in each column
            data1col = data1dft; % data for this column
            err = Inf;
            olderr = Inf;
            while err < stopthresh*olderr || err == Inf;%for jj = 1:niters
                % build the system matrix for this column
                A = exp(-1i*2*pi*(-dim/2:dim/2-1)'*x(mask(:,ii))+1i*tte(:)*real(mhybr(mask(:,ii),ii).'));
                Ai = bsxfun(@times,A,col(exp(-imag(mhybr(mask(:,ii),ii).'))).'.*col(imgc(mask(:,ii))).');
                aux = data1col-sum(Ai,2);
                % calculate err
                olderr = err;
                err = norm(aux);
                % calculate gradient wrt temp map
                gr = real(2*1i*(Ai'*(tte(:).*aux)));
                gi = imag(2*1i*(Ai'*aux));
                % step in negative gradient direction
                mhybr(mask(:,ii),ii) = mhybr(mask(:,ii),ii) - step*(gr+1i*gi);
                mhybr(mask(:,ii),ii) = real(mhybr(mask(:,ii),ii)).*(real(mhybr(mask(:,ii),ii)) < 0) + ...
                    1i*imag(mhybr(mask(:,ii),ii)).*(imag(mhybr(mask(:,ii),ii)) > 0);
            end
            
        end
        
        mhybr_all(:,:,ind) = mhybr;
        mhybrsv_all(:,:,ind) = mhybrsv;
        
    end
    toc
end
