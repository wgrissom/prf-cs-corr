#define USEKISSFFT
#define MWBLAS

#include <math.h>
#include "mex.h"
#include "blas.h"

/* Build using: */
/* 'mex thermocscorr_mex_rth.c -lblas' */
/* or */
/* 'mex thermocscorr_mex_rth.c -lmwblas' if MWBLAS is defined above, and blas.h is NOT in the current directory */

#ifdef USEKISSFFT
#include "kiss_fft.h"
#endif

#define PI (3.141592653589793)

float norm2c(int n,double* inr,double* ini){
    int i;
    double out = 0;
    for(i = 0;i < n;i++){
        out += inr[i]*inr[i] + ini[i]*ini[i];
    }
    return sqrt(out);
}

#ifndef USEKISSFFT
void four1(float data[], int nn, int isign);

/* FFT with fftshift and separate real/imag input/output */
void ft(float *ftbuffer, float *datar, float *datai, long n){
    
    int i;
    /* put data into fourier-transformable (real/imag-interleaved) format */
    for(i = 0;i < n;i++){
        ftbuffer[2*i] = datar[(i+n/2)%n];
        ftbuffer[2*i+1] = datai[(i+n/2)%n];
    }
    /* calculate data FT */
    four1(ftbuffer-1,n,-1);
    /* put FT'd data back into data vectors */
    for(i = 0;i < n;i++){
        datar[(i+n/2)%n] = ftbuffer[2*i];
        datai[(i+n/2)%n] = ftbuffer[2*i+1];
    }
    
}
#endif

void buildA(double *Ar, double *Ai, float *x, float *tte,
        float *mr, float *mi, float *baseimgr, float *baseimgi,
        double *auxr, double *auxi, long nk, long nx){
    
    int kmin = -nk/2;
    int i,j;
    double arg;
    double tmpr,tmpi;
    for(j = 0;j < nk;j++){ /* col counter (k-space) */
        
        for(i = 0;i < nx;i++){ /* row counter (space) */
            
            arg = -2*PI*(kmin + j)*x[i] + tte[j]*mr[i];
            
            tmpr = exp(-mi[i])*cos(arg);
            tmpi = exp(-mi[i])*sin(arg);
            
            Ar[j*nx+i] = tmpr*baseimgr[i] - tmpi*baseimgi[i];
            Ai[j*nx+i] = tmpi*baseimgr[i] + tmpr*baseimgi[i];
            auxr[j] -= Ar[j*nx+i];
            auxi[j] -= Ai[j*nx+i];
            
        }
    }
    
}

void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{

    float *tte;
    double *Ar,*Ai; /* A matrix */
    double *auxr,*auxi; /* aux = data - sum(A,2) */
    float *datar,*datai;
    
    double *min; /* real and imaginary parts of temp map */
    double *mrout,*miout;
    float *baseimgr,*baseimgi;
    double *gr,*gi; /* real and imaginary parts of derivatives */
    double step; /* step size */
    bool verbose;

    int i,j,k; /* counters for loops */
    /* forms of op(A) & op(B) to use in matrix multiplication */
#ifndef MWBLAS
    char chn = 'N'; /* not transposed */
    int nx,nk;
    int onei = 1;
#else
    char *chn = "N";
    long nx,nk;
    long onei = 1;
#endif
    double one = 1.0, zero = 0.0, two = 2.0, mintwo = -2.0;

    float err,olderr;
    double stopthresh;
    bool *mask;
    double *imghotr,*imghoti;
    float tmpr,tmpi;
    double *fr,*fi;
    float *xm;
    double c;
    float *mrm,*mim;
    double *maskin;
    
    float maskthresh;
    float tro,te;
    float cc,sc;
    
#ifdef USEKISSFFT
    kiss_fft_cfg fftcfg;
    kiss_fft_cpx *ftbuffer;
    kiss_fft_cpx *ftbufferout;
#else
    float *ftbuffer;
#endif
    
    if (nrhs != 11)
        mexErrMsgTxt("Input arguments: ");
    if (nlhs != 1)
        mexErrMsgTxt("Output arguments: m");
    
    /* process input args */
    if(!mxIsDouble(prhs[0]))
        mexErrMsgTxt("input m must be double precision");
    min = (double *)mxGetPr(prhs[0]);
    step = (double)mxGetScalar(prhs[1]);
    nk = mxGetM(prhs[0]);
    imghotr = (double *)mxGetPr(prhs[2]);
    imghoti = (double *)mxGetPi(prhs[2]);
    stopthresh = (double)mxGetScalar(prhs[3]);
    fr = (double *)mxGetPr(prhs[4]);
    fi = (double *)mxGetPi(prhs[4]);
    c = (double)mxGetScalar(prhs[5]);
    
    /* mask threshold */
    maskthresh = (double)mxGetScalar(prhs[6]);
    maskin = (double *)mxGetPr(prhs[7]);
    
    tro = (double)mxGetScalar(prhs[8]);
    te = (double)mxGetScalar(prhs[9]);
    verbose = mxGetScalar(prhs[10]) == 1.0;

    /* allocate space for result */
    plhs[0] = mxCreateDoubleMatrix(nk,nk,mxCOMPLEX);
    mrout = mxGetPr(plhs[0]);
    miout = mxGetPi(plhs[0]);
    
    mask = (bool *)malloc(nk*sizeof(bool));
    auxr = (double *)malloc(nk*sizeof(double));
    auxi = (double *)malloc(nk*sizeof(double));
    datar = (float *)malloc(nk*sizeof(float));
    datai = (float *)malloc(nk*sizeof(float));
    xm = (float *)malloc(nk*sizeof(float));
    baseimgr = (float *)malloc(nk*sizeof(float));
    baseimgi = (float *)malloc(nk*sizeof(float));
    mrm = (float *)malloc(nk*sizeof(float));
    mim = (float *)malloc(nk*sizeof(float));
    Ar = (double *)malloc(nk*nk*sizeof(double));
    Ai = (double *)malloc(nk*nk*sizeof(double));
    gr = (double *)malloc(nk*sizeof(double));
    gi = (double *)malloc(nk*sizeof(double));
    tte = (float *)malloc(nk*sizeof(float));

#ifndef USEKISSFFT
    ftbuffer = (float *)malloc(2*nk*sizeof(float));
#else
    ftbuffer = (kiss_fft_cpx *)malloc(nk*sizeof(kiss_fft_cpx));
    ftbufferout = (kiss_fft_cpx *)malloc(nk*sizeof(kiss_fft_cpx));
    fftcfg = kiss_fft_alloc(nk,0,NULL,NULL);
#endif

    int nxtotal = 0;
    int itertotal = 0;
    
    for(i = 0;i < nk;i++)
        tte[i] = (-nk/2+i)/(float)(nk)*(tro/te)+1.0;
    
    cc = cos(c);sc = sin(c);    
    
    for(k = 0;k < nk;k++){ /* loop over columns */
        
        nx = 0;
        for(i = 0;i < nk;i++){
            mask[i] = (min[k*nk+i] < maskthresh) || (maskin[k*nk+i] == 1.0);
            nx += (int)mask[i];
        }
        nxtotal += nx;

        if(nx > 0){
            
            /* copy the mask */
            j = 0;
            for(i = 0;i < nk;i++)
                if(mask[i])
                    xm[j++] = (float)(-nk/2+i)/(float)(nk);
            
            /* calculate the baseline image in the mask */
            j = 0;
            for(i = 0;i < nk;i++)
                if(mask[i]){
                    baseimgr[j] = fr[k*nk+i]*cc - fi[k*nk+i]*sc;
                    baseimgi[j] = fi[k*nk+i]*cc + fr[k*nk+i]*sc;
                    j++;
                }
		
            /* data1dft = imghot(:,ii) - ~mask(:,ii).*imgc.*exp(1i*mhybr(:,ii)); */
            for(i = 0;i < nk;i++){
                datar[i] = imghotr[k*nk+i];
                datai[i] = imghoti[k*nk+i];
                if(!mask[i]){
                    cc = cos(min[k*nk+i]+c);sc = sin(min[k*nk+i]+c);
                    datar[i] -= fr[k*nk+i]*cc - fi[k*nk+i]*sc;
                    datai[i] -= fi[k*nk+i]*cc + fr[k*nk+i]*sc;
                }
            }

            /* data1dft = ft(data1dft); */
#ifndef USEKISSFFT
            ft(ftbuffer,datar,datai,nk);
#else
            for(i = 0;i < nk;i++){
                ftbuffer[i].r = datar[(i+nk/2)%nk];
                ftbuffer[i].i = datai[(i+nk/2)%nk];
            }
            kiss_fft( fftcfg , ftbuffer , ftbufferout );
            for(i = 0;i < nk;i++){
                datar[(i+nk/2)%nk] = ftbufferout[i].r;
                datai[(i+nk/2)%nk] = ftbufferout[i].i;
            }
#endif
            
            /* copy m -> m_masked, in masked locations */
            j = 0;
            for(i = 0;i < nk;i++)
                if(mask[i]){
                    mrm[j] = min[k*nk+i];
                    mim[j] = 0;
                    j++;
                }
            
            olderr = -1;
            err = -1;
            while((err < stopthresh*olderr) || (err == -1) || (olderr == -1)){
                
                /* A = exp(-1i*2*pi*(-dim/2:dim/2-1)'*x+1i*(tte*real(m(:)).')); */
                /* A = bsxfun(@times,A,exp(-imag(m(:))).'.*baseimg(:).'); */
                /* aux = data-sum(A,2); */
                for(i = 0;i < nk;i++){
                    auxr[i] = datar[i];
                    auxi[i] = datai[i];
                }
                buildA(Ar, Ai, xm, tte, mrm, mim, baseimgr, baseimgi, auxr, auxi, nk, nx);
                
                /* err = norm(aux) */
                olderr = err;
                err = norm2c(nk,auxr,auxi);

#ifndef MWBLAS
                /* gi = real(2*(A'*aux)); */
                dgemv_(&chn, &nx, &nk, &one, Ar, &nx, auxr, &onei, &zero, gi, &onei);
                dgemv_(&chn, &nx, &nk, &two, Ai, &nx, auxi, &onei, &two, gi, &onei);
                /* gr = -imag(2*(A'*(tte.*aux))); */
                for(i = 0;i < nk;i++){
                    auxr[i] *= tte[i];
                    auxi[i] *= tte[i];
                }
                dgemv_(&chn, &nx, &nk, &one, Ar, &nx, auxi, &onei, &zero, gr, &onei);
                dgemv_(&chn, &nx, &nk, &two, Ai, &nx, auxr, &onei, &mintwo, gr, &onei);
#else
                /* gi = real(2*(A'*aux)); */
                dgemv(chn, &nx, &nk, &one, Ar, &nx, auxr, &onei, &zero, gi, &onei);
                dgemv(chn, &nx, &nk, &two, Ai, &nx, auxi, &onei, &two, gi, &onei);
                /* gr = -imag(2*(A'*(tte.*aux))); */
                for(i = 0;i < nk;i++){
                    auxr[i] *= tte[i];
                    auxi[i] *= tte[i];
                }
                dgemv(chn, &nx, &nk, &one, Ar, &nx, auxi, &onei, &zero, gr, &onei);
                dgemv(chn, &nx, &nk, &two, Ai, &nx, auxr, &onei, &mintwo, gr, &onei);
#endif		
                for(i = 0;i < nx;i++){
                    
                    /* step in negative deriv direction */
                    mrm[i] -= step*gr[i];
                    mim[i] -= step*gi[i];
                    
                    /* enforce negative real part, positive imag part */
                    mrm[i] = mrm[i] < 0 ? mrm[i] : 0;
                    mim[i] = mim[i] > 0 ? mim[i] : 0;
                    
                }

                itertotal++;
		
            }
            
        } /* if nx > 0 */

	/* copy masked result to full vector */
	j = 0;
	for(i = 0;i < nk;i++){
	  if(!mask[i]){
	    mrout[k*nk+i] = min[k*nk+i];
	    miout[k*nk+i] = 0;
	  } else {
	    mrout[k*nk+i] = mrm[j];
	    miout[k*nk+i] = mim[j];
	    j++;
	  }
	}

    } /* loop over columns */
    
    free(baseimgr);
    free(baseimgi);
    free(xm);
    free(datar);
    free(datai);
    free(auxr);
    free(auxi);
    free(mask);
    free(mrm);
    free(mim);
    free(Ar);
    free(Ai);
    free(gr);
    free(gi);
    free(tte);
    free(ftbuffer);
#ifdef USEKISSFFT
    free(fftcfg);
    free(ftbufferout);
#endif

	if(verbose)
        printf("Total voxels corrected: %d. Total iterations: %d.\n",nxtotal,itertotal);
    
    return;
}

#ifndef USEKISSFFT
#include "four1.c"
#else
#include "kiss_fft.c"
#endif
