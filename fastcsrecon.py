#!/usr/bin/python

import os
import scipy.io as sio
import numpy as np
import logging

datadir = os.getcwd()
fname = os.path.join(datadir,"fastcs2dftdata.mat")
logfile = os.path.join(datadir,"pylog.txt")
logging.basicConfig(filename=logfile,level=logging.DEBUG)

matfile = sio.loadmat(fname)

step = matfile['step']
mask = matfile['mask']
m = matfile['mhybrsv']
m = m + 0j #complex
# assuming 1 coil for now
te = matfile['te']
dim = mask.shape
ti = np.arange(-1*matfile['tro']/2,matfile['tro']/2,matfile['tro']/dim[0])
x = np.arange(-0.5,0.5,dim[0]**-1)
imgbase = np.multiply(matfile['fhybr'],np.exp(1j*(matfile['Ahybr'].reshape(dim))*matfile['chybr']))

cols = np.squeeze(np.nonzero(np.sum(mask,0)))
notmask = mask < 1
notmask = notmask.astype('uint8')
data1dft = matfile['imghot']-np.multiply(notmask,np.multiply(imgbase,np.exp(1j*m)))
# data from outside hot spot; ft the frequency-encoded dimension to go back to k-space data
data1dft = np.fft.fftshift(np.fft.fft(np.fft.fftshift(data1dft),axis = 0))

logging.info('Starting CS compensation')

for col in cols:
    # iteratively update the map inside the hot spot in each column
    data1col = np.squeeze(data1dft[:,col])
    err = np.inf
    olderr = np.inf
    while err < 0.999*olderr or err == np.inf:
        # build the system matrix for this column
        tmp1 = np.arange(-dim[0]/2,dim[0]/2)
        tmp2 = (tmp1[...,np.newaxis])*x[np.nonzero(mask[:,col])].T
        tmp3 = (np.ravel(ti)/te+1).T*np.real(m[np.nonzero(mask[:,col]),col])
        A = np.exp(-1j*2*np.pi*tmp2+1j*tmp3)
        Ai = np.multiply(A,np.multiply(np.ravel(np.exp(-np.imag(m[np.nonzero(mask[:,col]),col].T)).T),np.ravel(imgbase[np.nonzero(mask[:,col]),col].T)))
        aux = np.ravel(data1col.T) - np.sum(Ai,1)
        # calculate error
        olderr = err
        err = np.linalg.norm(aux)
        # calculate gradient wrt temp map
        gr = np.real(2*1j*np.dot(Ai.conj().T,np.multiply((ti/te+1),aux).T))
        gi = np.imag(2*1j*(np.dot(Ai.conj().T,aux)))
        # step in negative gradient direction
        m[np.nonzero(mask[:,col]),col] = m[np.nonzero(mask[:,col]),col] - step*(gr+1j*gi[...,np.newaxis]).T
        m[np.nonzero(mask[:,col]),col] = np.multiply(np.real(m[np.nonzero(mask[:,col]),col]),np.real(m[np.nonzero(mask[:,col]),col]) < 0) + 1j*np.multiply(np.imag(m[np.nonzero(mask[:,col]),col]),np.imag(m[np.nonzero(mask[:,col]),col]) > 0)
        # write err in log file
        logging.info('Col: %s Iteration: %s Err: %f', col, niter, err)

matfile['mcs'] = m
fname = os.path.join(datadir,"fastcs2dftdata_py.mat")
sio.savemat(fname,matfile)
