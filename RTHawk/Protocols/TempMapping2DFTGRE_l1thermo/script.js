var sequenceId = rth.sequenceId();

var scannerParameters = new RthUpdateGetParametersCommand(sequenceId);
rth.addCommand(scannerParameters);
var parameterList = scannerParameters.receivedData();


// Minimum TR calculations
var scannerTR = new RthUpdateGetTRCommand(sequenceId, [], []);
rth.addCommand(scannerTR);
var minTR = scannerTR.tr();

var minTE = SB.excitation['<Slice Select Gradient>.end'] - SB.excitation['<Slice Select Gradient>.plateauRefocusPoint'] + SB.readout['<Cartesian Readout>.readoutCenter'] - SB.readout['<Cartesian Readout>.start'];

var sampRate = SB.readout['<Cartesian Readout>.samplingRate']; // ksamp/s
var tReadout = SB.readout['<Cartesian Readout>.readoutEnd'] - SB.readout['<Cartesian Readout>.readoutStart']; // ms

// Reconstruction parameters
var frameRate = 30;  // Desired frame rate [fps]
var viewsPerReconstruction = Math.max(1, Math.floor(1000.0 / (frameRate * minTR)));
var gamma = SB.excitation['gamma']; // [Hz/G]
var fieldStrength = SB.excitation['fieldStrength']; // [T]
var alpha = 0.01; // temperature coefficient [ppm/degC], From Peters 1998, "Ex Vivo Tissue-Type Independence in MR Thermometry"
var scaleToDegC = 100000 / (alpha * gamma * fieldStrength * minTE * 2 * Math.PI);

// Information annotation
var instanceName = rth.instanceName();
rth.addSeriesDescription(instanceName);
rth.addSeriesDescription("Axial " + instanceName);
rth.addSeriesDescription("Short Axis " + instanceName);
rth.addSeriesDescription("2-Chamber " + instanceName);
rth.addSeriesDescription("3-Chamber " + instanceName);
rth.addSeriesDescription("4-Chamber " + instanceName);
rth.informationInsert(sequenceId, "mri.SequenceName", "RT 2DFT GRE");
rth.informationInsert(sequenceId, "mri.RepetitionTime", minTR);
rth.informationInsert(sequenceId, "mri.EchoTime", minTE);
rth.informationInsert(sequenceId, "mri.PreferredPlaybackSequencing", 0);

rth.informationInsert(sequenceId, "mri.ScanningSequence", "GR");
rth.informationInsert(sequenceId, "mri.SequenceVariant", "SS");
rth.informationInsert(sequenceId, "mri.ScanOptions", "");
rth.informationInsert(sequenceId, "mri.MRAcquisitionType", "2D");
rth.informationInsert(sequenceId, "mri.NumberOfAverages", 1);
rth.informationInsert(sequenceId, "mri.EchoTrainLength", 1);

// Configure interactive tools
var navigation = new RthNavigationTool(rth.imageDisplay("main"));
navigation.acceptsDraggedPrescriptions = true;
rth.addTool(navigation);
navigation.geometryChanged.connect(rth.setGeometry);

var navigation_ax = new RthNavigationTool(rth.imageDisplay("axial"));
navigation_ax.immediateMode = false;
rth.addTool(navigation_ax);
navigation_ax.cutChanged.connect(rth.setPrescribedGeometry);
navigation_ax.geometryChanged.connect(rth.setGeometry);

var navigation_sa = new RthNavigationTool(rth.imageDisplay("sagittal"));
navigation_sa.immediateMode = false;
rth.addTool(navigation_sa);
navigation_sa.cutChanged.connect(rth.setPrescribedGeometry);
navigation_sa.geometryChanged.connect(rth.setGeometry);

var navigation_co = new RthNavigationTool(rth.imageDisplay("coronal"));
navigation_co.immediateMode = false;
rth.addTool(navigation_co);
navigation_co.cutChanged.connect(rth.setPrescribedGeometry);
navigation_co.geometryChanged.connect(rth.setGeometry);

navigation.geometryChanged.connect(navigation_ax.setCutGeometry);
navigation.geometryChanged.connect(navigation_sa.setCutGeometry);
navigation.geometryChanged.connect(navigation_co.setCutGeometry);

var contrast = new RthContrastTool(rth.imageDisplay("main"));
rth.addTool(contrast);

var contrast_ax = new RthContrastTool(rth.imageDisplay("axial"));
rth.addTool(contrast_ax);

var contrast_sa = new RthContrastTool(rth.imageDisplay("sagittal"));
rth.addTool(contrast_sa);

var contrast_co = new RthContrastTool(rth.imageDisplay("coronal"));
rth.addTool(contrast_co);

var temperature = new RthVelocityContrastTool(rth.imageDisplay("main"));
temperature.setUnitsLabel(qsTr("°C"));
temperature.setTooltipLabel(qsTr("Temperature Change"));
temperature.setScale(-Math.PI * scaleToDegC, Math.PI * scaleToDegC);
rth.addTool(temperature);

var annotation = new RthAnnotationTool(rth.imageDisplay("main"));
rth.addTool(annotation);


var separator1 = new RthToolSeparator(rth.imageDisplay("main"));
rth.addTool(separator1);

var axialView = new RthStandardViewsTool(rth.imageDisplay("main"),'Axial');
rth.addTool(axialView);
axialView.geometryChanged.connect(rth.setGeometry);

var sagittalView = new RthStandardViewsTool(rth.imageDisplay("main"),'Sagittal');
rth.addTool(sagittalView);
sagittalView.geometryChanged.connect(rth.setGeometry);

var coronalView = new RthStandardViewsTool(rth.imageDisplay("main"),'Coronal');
rth.addTool(coronalView);
coronalView.geometryChanged.connect(rth.setGeometry);

var separator2 = new RthToolSeparator(rth.imageDisplay("main"));
rth.addTool(separator2);

var radiologicView = new RthStandardViewsTool(rth.imageDisplay("main"),'Radiologic');
rth.addTool(radiologicView);
radiologicView.geometryChanged.connect(rth.setGeometry);

rth.newPrescribedGeometry.connect(function(geom) {
  navigation_ax.setCutGeometry(geom);
  navigation_sa.setCutGeometry(geom);
  navigation_co.setCutGeometry(geom);
});
rth.newApplicationGeometry.connect(function(geom) {
  rth.activateScanButton();
});

// Data source.
var observer = new RthReconRawObserver(); 
observer.objectName = "Observer";
observer.setSequenceId(sequenceId);
// Just to be safe
observer.setReadoutName("readout");
observer.setViewIndexKey("<Cartesian Readout>.index");
observer.setSamples(SB.readout['<Cartesian Readout>.samples']);

function reconstructionBlock(input, index) {
    this.sort = new RthReconRawToImageSort();
    this.sort.objectName = "Sort(" + index + ")";
    this.sort.samples = SB.readout['<Cartesian Readout>.samples'];
    this.sort.phaseEncodes = SB.readout['<Repeat>.repetitions'];
    this.sort.observeValueForKey("reconstruction.viewsPerReconstruction", "accumulate");
    this.sort.setInput(input);

    this.fft = new RthReconImageFFT();
    this.fft.objectName = "FFT(" + index + ")";
    this.fft.setInput(this.sort.output());

    this.baselineSplit = new RthReconImageSplitter();
    this.baselineSplit.objectName = "BaselineSplit(" + index + ")";
    this.baselineSplit.setSequential(true); // must process outputs sequentially to avoid re-entrant errors with persistent baseline input
    this.baselineSplit.setInput(this.fft.output());

    var that = this;
    this.switchBaseline = new RthReconImagePassThrough();
    this.switchBaseline.objectName = "SwitchBaseline(" + index + ")";
    this.switchBaseline.setInput(this.baselineSplit.output(0));
    this.switchBaseline.observeMultipleKeys(["reconstruction.viewsPerReconstruction", "reconstruction.firstAcquisition", "mri.EchoTime", "mri.FlipAngle", "mri.RepetitionTime"]);
    this.switchBaseline.observedKeysChanged.connect(function(keys){
      var firstRun = keys["reconstruction.firstAcquisition"];
      if(firstRun) {
	// Needs to output an image when run the first time
	that.switchBaseline.setEnableCount(0); 
	rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "firstAcquisition", 0));
      } else {
	// Wait for sliding window transients to die out.
	that.switchBaseline.setEnableCount(Math.ceil(SB.readout['<Repeat>.repetitions'] / keys["reconstruction.viewsPerReconstruction"]));
	that.switchBaseline.activate();
      }
    });
    this.switchBaseline.setDisableCount(1);
    this.switchBaseline.geometryChanged.connect(this.switchBaseline.activate);

    //this.conjugate = new RthReconImageConj();
    //this.conjugate.setInput(this.baselineSplit.output(1));
    //this.conjugate.objectName = "Conjugate(" + index + ")";

    //this.switchBaselineSplit = new RthReconImageSplitter();
    //this.switchBaselineSplit.setInput(this.switchBaseline.output());
    //this.switchBaselineSplit.objectName = "Switch Baseline Splitter(" + index + ")";
    //this.switchBaselineSplit.setPersistentInput(0);

    //var that = this;
    //this.multiply = RthReconImageArrayMultiply();
    //this.multiply.setInput(0, conjugate = new RthReconImageConj();this.switchBaselineSplit.output(0)); 
    //this.multiply.setInput(1, this.conjugate.output());
    //this.multiply.setPersistentInput(0);
    //this.multiply.objectName = "Multiply(" + index + ")";
    
    this.output = function() {return this.baselineSplit.output(1);}
    this.output2 = function() {return this.baselineSplit.output(2);}
    this.baselineoutput = function() {return this.switchBaseline.output();}
    //this.temperatureOutput = function() {return this.multiply.output();}
}

var sumOfSquares = new RthReconImageSumOfSquares();
sumOfSquares.objectName = "SoS";

//var complexSum = new RthReconImageAdd();
//complexSum.objectName = "Sum";

var dynamicImgSum = new RthReconImageAdd();
dynamicImgSum.objectName = "Dynamic Img Sum";

var baselineImgSum = new RthReconImageAdd();
baselineImgSum.objectName = "Baseline Img Sum";

var unwarp = RthReconImageGradWarp();
unwarp.objectName = "Unwarp";
unwarp.setKernelRadius(2);
unwarp.observeValueForKey("equipment.Signa/GradWarp/scalex3", "scaleX3");
unwarp.observeValueForKey("equipment.Signa/GradWarp/scalex5", "scaleX5");
unwarp.observeValueForKey("equipment.Signa/GradWarp/scaley3", "scaleY3");
unwarp.observeValueForKey("equipment.Signa/GradWarp/scaley5", "scaleY5");
unwarp.observeValueForKey("equipment.Signa/GradWarp/scalez3", "scaleZ3");
unwarp.observeValueForKey("equipment.Signa/GradWarp/scalez5", "scaleZ5");
unwarp.setInput(sumOfSquares.output());

var abs = RthReconImageAbs();
abs.objectName = "Abs";
abs.setInput(unwarp.output());

//var angle = new RthReconImageArg();
//angle.setInput(unwarpPhase.output());
//angle.setInput(complexSum.output());

//var scalecorr = new RthReconImageScale();
//scalecorr.setName = "Negate Phase before corr";
//scalecorr.setScaling(-1.0);
//scalecorr.setInput(angle.output());

var conjugatebaseline = new RthReconImageConj();
conjugatebaseline.setInput(baselineImgSum.output());
conjugatebaseline.setPersistentInput(0);

var conjugatedynamic = new RthReconImageConj();
conjugatedynamic.setInput(dynamicImgSum.output());

qt.script.importExtension("wag.scriptPlugin");

var baselinemask = new ellipsemask();
baselinemask.setInput(0,conjugatebaseline.output());
baselinemask.setPersistentInput(0);

var dynmask = new ellipsemask();
dynmask.setInput(0,conjugatedynamic.output());

var dispimgmask = new ellipsemask();
dispimgmask.setInput(0,abs.output());

var combinedBaselineSplit = new RthReconImageSplitter();
combinedBaselineSplit.setInput(0,baselinemask.output());
combinedBaselineSplit.setPersistentInput(0);

var baselineExport = new RthReconImageExport();
baselineExport.objectName = 'baselineExport';
baselineExport.observeMultipleKeys(["series.SeriesDescription", "reconstruction.temporalPhase", "acquisition.Slice", "series.SeriesNumber"]);
//baselineExport.observeMultipleKeys(["reconstruction.temporalPhase"]);
baselineExport.observedKeysChanged.connect(function(keys){
  slice = keys["acquisition.Slice"];
  desc = keys["series.SeriesDescription"];
  seriesNum = keys["series.SeriesNumber"];
  phase = keys["reconstruction.temporalPhase"];
  phasestr = ("00" + phase).slice(-2); // NOTE this only works for first 2 digits
  newfile = "baselineimg_" + phasestr + ".raw"
  RTHLOGGER_WARNING("Changing filename to " + newfile); // TODO remove line
  baselineExport.setFileName(newfile);
});
baselineExport.setInput(0,combinedBaselineSplit.output(1));
baselineExport.setPersistentInput(0);
//baselineExport.setFileName('baselineimg');

var combinedDynSplit = new RthReconImageSplitter();
combinedDynSplit.setInput(0,dynmask.output());

var dynExport = new RthReconImageExport();
dynExport.objectName = 'dynExport';
dynExport.observeMultipleKeys(["series.SeriesDescription", "reconstruction.temporalPhase", "acquisition.Slice", "series.SeriesNumber"]);
//baselineExport.observeMultipleKeys(["reconstruction.temporalPhase"]);
dynExport.observedKeysChanged.connect(function(keys){
  slice = keys["acquisition.Slice"];
  desc = keys["series.SeriesDescription"];
  seriesNum = keys["series.SeriesNumber"];
  phase = keys["reconstruction.temporalPhase"];
  phasestr = ("00" + phase).slice(-2); // NOTE this only works for first 2 digits
  slicestr = ("00" + slice).slice(-2); // NOTE this only works for first 2 digits
  seriesstr = ("000" + seriesNum).slice(-3); // NOTE this only works for first 3 digits
  newfile = desc.replace(/ /g,'') + "+series" + seriesstr + "-slice" + slicestr + "-phase" + phasestr + ".raw"
  RTHLOGGER_WARNING("Changing filename to " + newfile); // TODO remove line
  dynExport.fileName = newfile;
});
dynExport.setInput(0,combinedDynSplit.output(1));
//dynExport.setFileName('dynimg');

var thermohybridl1 = new thermohybrid();
thermohybridl1.setInput(0,combinedBaselineSplit.output(0));
thermohybridl1.setPersistentInput(0);
thermohybridl1.setInput(1,combinedDynSplit.output(0));
thermohybridl1.setVerbose(true);

var tempSplit = new RthReconImageSplitter();
tempSplit.setInput(0,thermohybridl1.output());

var tempExport = new RthReconImageExport();
tempExport.setInput(0,tempSplit.output(0));
tempExport.setFileName('tempimg');

var unwarpPhase = new RthReconImageGradWarp();
unwarpPhase.objectName = "UnwarpPhase";
unwarpPhase.setKernelRadius(2);
unwarpPhase.observeValueForKey("equipment.Signa/GradWarp/scalex3", "scaleX3");
unwarpPhase.observeValueForKey("equipment.Signa/GradWarp/scalex5", "scaleX5");
unwarpPhase.observeValueForKey("equipment.Signa/GradWarp/scaley3", "scaleY3");
unwarpPhase.observeValueForKey("equipment.Signa/GradWarp/scaley5", "scaleY5");
unwarpPhase.observeValueForKey("equipment.Signa/GradWarp/scalez3", "scaleZ3");
unwarpPhase.observeValueForKey("equipment.Signa/GradWarp/scalez5", "scaleZ5");
unwarpPhase.setInput(tempSplit.output(1));

var scale = new RthReconImageScale();
scale.setName = "Negate Phase";
scale.setScaling(-1.0);
scale.setInput(unwarpPhase.output());

rth.importJS("lib:RthImageThreePlaneOutput.js");

//var magThreePlane = new RthImageThreePlaneOutput();
//magThreePlane.objectName = "Mag Out";
//magThreePlane.setInput(abs.output());

var mainImage = new RthReconImageToRthDisplayImage();
mainImage.setInput(scale.output());
mainImage.newImage.connect(rth.imageBuffer("main").appendImage);

var axialImage = new RthReconImageToRthDisplayImage();
axialImage.setInput(dispimgmask.output());
axialImage.newImage.connect(rth.imageBuffer("axial").appendImage);
rth.imageBuffer("axial").saveEnabled = false;


//var phsThreePlane = new RthImageThreePlaneOutput();
//phsThreePlane.objectName = "Phs Out";
//phsThreePlane.setInput(scale.output());

//var image = new RthReconImageColorFlowToRthDisplayImage();
//var image = new RthReconImageDisplayImage();
//image.objectName = "Image";
//image.setInput(0, abs.output());
//image.setInput(1, scale.output());
//image.newImage.connect(rth.imageBuffer("main").appendImage);

//rth.importJS("lib:RthImageThreePlaneOutput.js");
//var threePlane = new RthImageThreePlaneOutput();
//threePlane.objectName = "ThreePlane";
//threePlane.setInput(abs.output());

var block = new Array();

function connectCoils(coils) {
    for(var i = 0; i < coils; i++) {
	block[i] = new reconstructionBlock(observer.output(i), i);
	sumOfSquares.setInput(i, block[i].output());
	dynamicImgSum.setInput(i, block[i].output2());
	baselineImgSum.setInput(i, block[i].baselineoutput());
	baselineImgSum.setPersistentInput(i);
        //complexSum.setInput(i, block[i].temperatureOutput());
        controlWidget.pushButton_ResetBaseline.clicked.connect(block[i].switchBaseline.activate);
    }
}

observer.coilsChanged.connect(connectCoils);




rth.updateSharedParameter = function(key, value) {
    if(key == "shims") {
	rth.addCommand(new RthUpdateShimsCommand(sequenceId, value[0], value[1], value[2], value[3]));
    }
}



var startingTip = SB.excitation["<Sinc RF>.tip"];

function changeFlipAngle(angle) {
  if (angle > startingTip) angle=startingTip;
  rth.addCommand(new RthUpdateFloatParameterCommand(sequenceId, "excitation", "scaleRF", "", angle/startingTip));
  rth.addCommand(new RthUpdateChangeMRIParameterCommand(sequenceId, "FlipAngle", angle));
}

controlWidget.inputWidget_FlipAngle.valueChanged.connect(changeFlipAngle);
controlWidget.inputWidget_FlipAngle.minimum = 0;
controlWidget.inputWidget_FlipAngle.maximum = startingTip;
controlWidget.inputWidget_FlipAngle.value = startingTip;
changeFlipAngle(controlWidget.inputWidget_FlipAngle.value);


var startingThickness = SB.excitation['<Slice Select Gradient>.thickness'];
var currentThickness = startingThickness;
var currentFovScale = 1;

function changeSliceThickness(thickness) {
  if(thickness < startingThickness) thickness = startingThickness;
  rth.addCommand(new RthUpdateScaleGradientsCommand(sequenceId, "excitation", 1 / currentFovScale, 1 / currentFovScale, startingThickness/thickness));
  rth.addCommand(new RthUpdateChangeSliceThicknessCommand(sequenceId, thickness));
  navigation.setSliceThickness(thickness);
  navigation_ax.setSliceThickness(thickness);
  navigation_sa.setSliceThickness(thickness);
  navigation_co.setSliceThickness(thickness);
  currentThickness = thickness;
}

controlWidget.inputWidget_SliceThickness.valueChanged.connect(changeSliceThickness);
controlWidget.inputWidget_SliceThickness.minimum = startingThickness;
controlWidget.inputWidget_SliceThickness.maximum = startingThickness * 4.0;
controlWidget.inputWidget_SliceThickness.value = startingThickness;
changeSliceThickness(controlWidget.inputWidget_SliceThickness.value);

var xFov = SB.readout['<Cartesian Readout>.fov'] * 10;
var yFov = Math.round(xFov * SB.readout['<yFovFraction>.value']);

function scaleFOV(fovScale) {
    var scale = fovScale / 100;
    rth.addCommand(new RthUpdateChangeFieldOfViewCommand(sequenceId, xFov * scale, yFov * scale, currentThickness));
    rth.addCommand(new RthUpdateChangeResolutionCommand(sequenceId, xFov / SB.readout['<Cartesian Readout>.xRes'] * scale, (yFov / SB.readout['<Cartesian Readout>.yRes'] / SB.readout['<yFovFraction>.value'] * scale)));
    rth.addCommand(new RthUpdateScaleGradientsCommand(sequenceId, "readout", 1 / scale, 1 / scale, startingThickness/currentThickness));
    navigation.setXFOV(xFov * scale);
    navigation.setYFOV(yFov * scale);
    navigation_ax.setXFOV(xFov * scale);  
    navigation_ax.setYFOV(yFov * scale);
    navigation_sa.setXFOV(xFov * scale);
    navigation_sa.setYFOV(yFov * scale);
    navigation_co.setXFOV(xFov * scale);
    navigation_co.setYFOV(yFov * scale);
    currentFovScale = scale;
}

controlWidget.inputWidget_FOV.valueChanged.connect(scaleFOV);
controlWidget.inputWidget_FOV.minimum = 100;
controlWidget.inputWidget_FOV.maximum = 400;
controlWidget.inputWidget_FOV.value = 100;
changeSliceThickness(controlWidget.inputWidget_SliceThickness.value);


function changeTR(tr) {
  if(tr < minTR) tr = minTR;
  var value = tr * 1000;
  var trCommand = RthUpdateIntParameterCommand(sequenceId, "", "setDesiredTR", "", value);
  rth.addCommand(trCommand);
  rth.addCommand(new RthUpdateChangeMRIParameterCommand(sequenceId, "RepetitionTime", tr));  

  viewsPerReconstruction = Math.max(1, Math.floor(1000.0 / (frameRate * tr)));
  rth.addCommand(new RthUpdateChangeMRIParameterCommand(sequenceId, "FrameTime", viewsPerReconstruction * tr));
  rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "viewsPerReconstruction", viewsPerReconstruction));
}

controlWidget.inputWidget_TR.valueChanged.connect(changeTR);
controlWidget.inputWidget_TR.minimum = minTR;
controlWidget.inputWidget_TR.maximum = minTR + 200;
controlWidget.inputWidget_TR.value = minTR;
changeTR(controlWidget.inputWidget_TR.value);


function changeTE(te) {
  if (te < minTE) te = minTE;
  var value = (te - minTE) * 1000;
  var teCommand = RthUpdateIntParameterCommand(sequenceId, "echodelay", "setDelay", "", value);
  rth.addCommand(teCommand);
  rth.addCommand(new RthUpdateChangeMRIParameterCommand(sequenceId, "EchoTime", te));

  // Update phase to temperature scaling
  scaleToDegC = 100000 / (alpha * gamma * fieldStrength * te * 2 * Math.PI);
  temperature.setScale(-Math.PI * scaleToDegC, Math.PI * scaleToDegC);

  // TR may also change when TE changes. Update the minimum TR and, if necessary, the current TR value
  // If the TR slider is hidden, TR should always be set to the minimum value
 controlWidget.inputWidget_TR.minimum = minTR + te - minTE;
  if(controlWidget.inputWidget_TR.visible)
    controlWidget.inputWidget_TR.value = Math.max(minTR + te - minTE, controlWidget.inputWidget_TR.value);
  else
    controlWidget.inputWidget_TR.value = minTR + te - minTE;
}

controlWidget.inputWidget_TE.valueChanged.connect(changeTE);
controlWidget.inputWidget_TE.minimum = minTE;
controlWidget.inputWidget_TE.maximum = minTE + 100;
controlWidget.inputWidget_TE.value = minTE + 12;
changeTE(controlWidget.inputWidget_TE.value);

function changeMaskthresh(mt) {
  thermohybridl1.setMaskthresh(mt);
}

function changeLam(lam) {
  thermohybridl1.setLam(Math.pow(10, lam));
}
  
controlWidget.inputWidget_Maskthresh.valueChanged.connect(changeMaskthresh);
changeMaskthresh(controlWidget.inputWidget_Maskthresh.value);

controlWidget.inputWidget_Lam.valueChanged.connect(changeLam);
changeLam(controlWidget.inputWidget_Lam.value);

function changeEllMaskXDiameter(xdia) {

    baselinemask.setXdiameter(xdia);
    dynmask.setXdiameter(xdia);
    dispimgmask.setXdiameter(xdia);

}
function changeEllMaskYDiameter(ydia) {

    baselinemask.setYdiameter(ydia);
    dynmask.setYdiameter(ydia);
    dispimgmask.setYdiameter(ydia);

}
function changeEllMaskXOffset(xoff) {

    baselinemask.setXoffset(xoff);
    dynmask.setXoffset(xoff);
    dispimgmask.setXoffset(xoff);

}
function changeEllMaskYOffset(yoff) {

    baselinemask.setYoffset(yoff);
    dynmask.setYoffset(yoff);
    dispimgmask.setYoffset(yoff);

}
function changeEllMaskRotangle(rotangle) {

    baselinemask.setRotangle(rotangle);
    dynmask.setRotangle(rotangle);
    dispimgmask.setRotangle(rotangle);

}

controlWidget.inputWidget_ellipseMaskRotAngle.valueChanged.connect(changeEllMaskRotangle);
changeEllMaskRotangle(controlWidget.inputWidget_ellipseMaskRotAngle.value);

controlWidget.inputWidget_ellipseMaskXOffset.valueChanged.connect(changeEllMaskXOffset);
changeEllMaskXOffset(controlWidget.inputWidget_ellipseMaskXOffset.value);

controlWidget.inputWidget_ellipseMaskYOffset.valueChanged.connect(changeEllMaskYOffset);
changeEllMaskYOffset(controlWidget.inputWidget_ellipseMaskYOffset.value);

controlWidget.inputWidget_ellipseMaskXDiameter.valueChanged.connect(changeEllMaskXDiameter);
changeEllMaskXDiameter(controlWidget.inputWidget_ellipseMaskXDiameter.value);

controlWidget.inputWidget_ellipseMaskYDiameter.valueChanged.connect(changeEllMaskYDiameter);
changeEllMaskYDiameter(controlWidget.inputWidget_ellipseMaskYDiameter.value);

//function changeMagnitudeThreshold(value) {
//  image.setMagnitudeThreshold(value);
//}
//controlWidget.inputWidget_MagnitudeThreshold.valueChanged.connect(changeMagnitudeThreshold);

//function changeTemperatureThreshold(value) {
//  image.setVelocityThreshold(value);
//}
//controlWidget.inputWidget_TemperatureThreshold.valueChanged.connect(changeTemperatureThreshold);

controlWidget.inputWidget_MagnitudeThreshold.value = 13;
controlWidget.inputWidget_TemperatureThreshold.value = 13;

// Used to reset the baselineDelay after the initial acquisition
rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "firstAcquisition", 1));


