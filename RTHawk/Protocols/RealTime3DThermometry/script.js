// Real-Time 3D Redundant Spiral-In/Out GRE Sequence
// Written by Xue Feng
// 10/2014

var sequenceId = rth.sequenceId();
rth.informationInsert(sequenceId, "mri.SequenceName", "3D Redundant Spiral-In/Out GRE");

qt.script.importExtension("fus.scriptPlugin");

// Get the sequence parameters from the sequencer.
var scannerParameters = new RthUpdateGetParametersCommand(sequenceId);
rth.addCommand(scannerParameters);
var parameterList = scannerParameters.receivedData();
var minTR = parameterList[0];
rth.informationInsert(sequenceId, "mri.RepetitionTime", minTR);

//////////// SETUP TOOLS
var navigation = new RthNavigationTool(rth.imageDisplay("main"));
navigation.immediateMode = false;
navigation.acceptsDraggedPrescriptions = true;
navigation.cutChanged.connect(rth.setPrescribedGeometry);
rth.addTool(navigation);
//navigation.geometryChanged.connect(rth.setGeometry);

var navigation_ax = new RthNavigationTool(rth.imageDisplay("axial"));
navigation_ax.immediateMode = false;
rth.addTool(navigation_ax);
navigation_ax.cutChanged.connect(rth.setPrescribedGeometry);
//navigation_ax.geometryChanged.connect(rth.setGeometry);

var navigation_sa = new RthNavigationTool(rth.imageDisplay("sagittal"));
navigation_sa.immediateMode = false;
rth.addTool(navigation_sa);
navigation_sa.cutChanged.connect(rth.setPrescribedGeometry);
//navigation_sa.geometryChanged.connect(rth.setGeometry);

var navigation_co = new RthNavigationTool(rth.imageDisplay("coronal"));
navigation_co.immediateMode = false;
rth.addTool(navigation_co);
navigation_co.cutChanged.connect(rth.setPrescribedGeometry);
//navigation_co.geometryChanged.connect(rth.setGeometry);

//navigation.geometryChanged.connect(navigation_ax.setCutGeometry);
//navigation.geometryChanged.connect(navigation_sa.setCutGeometry);
//navigation.geometryChanged.connect(navigation_co.setCutGeometry);

var contrast = new RthContrastTool(rth.imageDisplay("main"));
rth.addTool(contrast);

var contrast_ax = new RthContrastTool(rth.imageDisplay("axial"));
rth.addTool(contrast_ax);

var contrast_sa = new RthContrastTool(rth.imageDisplay("sagittal"));
rth.addTool(contrast_sa);

var contrast_co = new RthContrastTool(rth.imageDisplay("coronal"));
rth.addTool(contrast_co);

var annotation = new RthAnnotationTool(rth.imageDisplay("main"));
rth.addTool(annotation);

var axialView = new RthStandardViewsTool(rth.imageDisplay("main"),'Axial');
rth.addTool(axialView);
axialView.geometryChanged.connect(rth.setGeometry);

var sagittalView = new RthStandardViewsTool(rth.imageDisplay("main"),'Sagittal');
rth.addTool(sagittalView);
sagittalView.geometryChanged.connect(rth.setGeometry);

var coronalView = new RthStandardViewsTool(rth.imageDisplay("main"),'Coronal');
rth.addTool(coronalView);
coronalView.geometryChanged.connect(rth.setGeometry);

var separator1 = new RthToolSeparator(rth.imageDisplay("main"));
rth.addTool(separator1);

var radiologicView = new RthStandardViewsTool(rth.imageDisplay("main"),'Radiologic');
rth.addTool(radiologicView);
radiologicView.geometryChanged.connect(rth.setGeometry);

rth.newPrescribedGeometry.connect(function(geom) {
  navigation_ax.setCutGeometry(geom);
  navigation_sa.setCutGeometry(geom);
  navigation_co.setCutGeometry(geom);
  navigation.setCutGeometry(geom);

  axialView.setCurrentGeometry(geom);
  sagittalView.setCurrentGeometry(geom);
  coronalView.setCurrentGeometry(geom);
  radiologicView.setCurrentGeometry(geom);
});

/////////////// Parameters
// spiral in and spiral out are symmetric so that the samples, interleaves, fov and resolution should be the same
var samples =  SB.sequence["<Spiral In Readout>.samples"];
var interleaves = SB.sequence["<Spiral In Readout>.interleaves"];
var zEncodes =  SB.sequence["<Z Encode>.res"];
var zEncodesSharing = zEncodes;
var sharingRatio = 1;
var acquisitions = interleaves*zEncodes;
var fieldOfView =  SB.sequence["<Spiral In Readout>.fov"] * 10;
var startingFieldOfView = fieldOfView;
var spiralInKey = "<Spiral In Readout>.index";
var spiralOutKey = "<Spiral Out Readout>.index";
var viewIndexKey = "<Spiral Repeat>.index";
var zIndexKey = "<Z Repeat>.index";
var zFieldOfView =  SB.sequence["<Z Encode>.fov"] * 10;
var startingZFieldOfView = zFieldOfView;
var resolution = startingFieldOfView / (Math.ceil(startingFieldOfView / SB.sequence["<Spiral In Readout>.spatialResolution"] / 2) * 2);
var startingTip = SB.sequence["<Sinc RF>.tip"];
// calculate TE
var TE = SB.sequence["<Time Interval>.center"] - SB.sequence["<Sinc RF>.peak"];
// insert information
var gamma = 267.52;
var alpha = 0.01;
var B0 = 2.99;
var scaling = 1.0 / (gamma * alpha * B0 * TE) * 1000.0;
rth.informationInsert(sequenceId, "mri.EchoTime", TE);

/////////////////// RECONSTRUCTION //////////////////////////////
// KSpace object to store kspace information
var spiralInKspace = new RthReconKSpace();
var spiralOutKspace = new RthReconKSpace();
spiralInKspace.loadFromReadoutTags(rth.readoutTags("sequence"), spiralInKey);
spiralOutKspace.loadFromReadoutTags(rth.readoutTags("sequence"), spiralOutKey);

// Gridding operator that caches the convolution table so the same
// is used for all coils.
var spiralInGriddingOperator = new RthReconGriddingOperator2D(spiralInKspace);
var spiralOutGriddingOperator = new RthReconGriddingOperator2D(spiralOutKspace);
var spiralInDeapodizationWindow = new RthReconImageSplitter();
var spiralOutDeapodizationWindow = new RthReconImageSplitter();
spiralInDeapodizationWindow.setInput(spiralInGriddingOperator.deapodizationWindow());
spiralOutDeapodizationWindow.setInput(spiralOutGriddingOperator.deapodizationWindow());

// Data source.
var observer = new RthReconRawObserver(); 
observer.objectName = "Raw Observer";
observer.setSequenceId(sequenceId);
observer.setViewIndexKey(viewIndexKey);
observer.setSliceIndexKey(zIndexKey);
observer.setSamples(samples);
observer.geometryChanged.connect(spiralInKspace.setGeometry);
observer.geometryChanged.connect(spiralOutKspace.setGeometry);
observer.observeMultipleKeys(["reconstruction.xGradientDelay", "reconstruction.yGradientDelay", "reconstruction.zGradientDelay"]);
observer.observedKeysChanged.connect(function(keys) { spiralInKspace.setXDelay(keys["reconstruction.xGradientDelay"]);   spiralInKspace.setYDelay(keys["reconstruction.yGradientDelay"]);  spiralInKspace.setZDelay(keys["reconstruction.zGradientDelay"]);});
observer.observedKeysChanged.connect(function(keys) { spiralOutKspace.setXDelay(keys["reconstruction.xGradientDelay"]);   spiralOutKspace.setYDelay(keys["reconstruction.yGradientDelay"]);  spiralOutKspace.setZDelay(keys["reconstruction.zGradientDelay"]);});

function adjustFramePosition(keys) {
  if (keys == 0 || keys["acquisition.<Overlay Combinations>.index"] == 0 || keys["reconstruction.lastData"] == 1) {
    rth.imageBuffer("main").displayBuffer().jumpToLastFrame();
    for (var i = 0; i < zEncodes / 2; i++) {
      rth.imageBuffer("main").displayBuffer().jumpToPreviousFrame();
    }
  }
}

// Macro block to reconstruct a single coil
// Note that local objects are declared with "var" so they
// are private to the macro block.
// If you want a sub module to be public the declare and
// use it as "this.object"
function reconBlock(input, index) {
  // Splitter for Rx Attenuator optimization blocks
  this.attenSplit = new RthReconRawSplitter();
  this.attenSplit.objectName = "Atten Split " + index;
  if (index == 0) {
    this.attenSplit.observeMultipleKeys(["acquisition.<Overlay Combinations>.index", "reconstruction.lastData", "geometry.TranslationZ"]);
    this.attenSplit.observedKeysChanged.connect(adjustFramePosition);
  }
  this.attenSplit.setInput(input);

  this.attenOutput = function() {
    return this.attenSplit.output(0);
  }

  this.spiralRouter = new RthReconRawRouteByIndex();
  this.spiralRouter.observeMultipleKeys(["acquisition.<Spiral In Readout>.index", "acquisition.<Spiral Out Readout>.index"]);
  this.spiralRouter.observedKeysChanged.connect(setSpiralOutput);
  this.spiralRouter.setInput(this.attenSplit.output(1));

  var that = this;
  function setSpiralOutput(keys) {
    if (typeof(keys["acquisition.<Spiral In Readout>.index"]) !== 'undefined') {
      that.spiralRouter.activeOutput = 0;
    } else if (typeof(keys["acquisition.<Spiral Out Readout>.index"]) !== 'undefined') {
      that.spiralRouter.activeOutput = 1;
    }
  }

  this.sliceInRouter = new RthReconRawRouteByIndex();
  this.sliceInRouter.observeMultipleKeys(["acquisition.<Z Repeat>.index"]);
  this.sliceInRouter.observedKeysChanged.connect(setSliceInOutput);
  this.sliceInRouter.setInput(this.spiralRouter.output(0));

  function setSliceInOutput(keys) {
    RTHLOGGER_INFO("slice in " + keys["acquisition.<Z Repeat>.index"] % zEncodesSharing);
    that.sliceInRouter.activeOutput = keys["acquisition.<Z Repeat>.index"] % zEncodesSharing;
  }

  this.sliceOutRouter = new RthReconRawRouteByIndex(); 
  this.sliceOutRouter.observeMultipleKeys(["acquisition.<Z Repeat>.index"]);
  this.sliceOutRouter.observedKeysChanged.connect(setSliceOutOutput);
  this.sliceOutRouter.setInput(this.spiralRouter.output(1));

  function setSliceOutOutput(keys) {
    RTHLOGGER_INFO("slice out " + keys["acquisition.<Z Repeat>.index"] % zEncodesSharing);
    that.sliceOutRouter.activeOutput = keys["acquisition.<Z Repeat>.index"] % zEncodesSharing;
  }

  this.pack3DSpiralIn = new RthReconImagePack();
  this.pack3DSpiralOut = new RthReconImagePack();

  this.sliceArray = new Array();

  this.resetSlice = function(sharingIndex) {
    this.pack3DSpiralIn.disconnectInputs();
    this.pack3DSpiralOut.disconnectInputs();
    this.sliceArray.length = 0;
    for (var sli = 0; sli < zEncodesSharing; sli++) {
      this.sliceArray[sli] = new reconSliceBlock(this.sliceInRouter.output(sli), this.sliceOutRouter.output(sli), sli);
      this.pack3DSpiralIn.setInput(sli, this.sliceArray[sli].spiralInOutput());
      this.pack3DSpiralOut.setInput(sli, this.sliceArray[sli].spiralOutOutput());
    }
  }

  this.viewSharingSpiralIn = new FUSReconImageViewSharing();
  this.viewSharingSpiralIn.setSharingRatio(sharingRatio);
  this.viewSharingSpiralIn.setInput(this.pack3DSpiralIn.output());
  this.viewSharingSpiralIn.objectName = "View Sharing Spiral In " + index;
  this.viewSharingSpiralIn.observeMultipleKeys(["acquisition.<Z Repeat>.index"]);
  this.viewSharingSpiralIn.observedKeysChanged.connect(setInViewSharingChunk);

  function setInViewSharingChunk(keys) {
    RTHLOGGER_INFO("view sharing chunk is " + Math.floor(keys["acquisition.<Z Repeat>.index"] / zEncodesSharing));
    that.viewSharingSpiralIn.viewChunk = Math.floor(keys["acquisition.<Z Repeat>.index"] / zEncodesSharing);
  }

  this.viewSharingSpiralOut = new FUSReconImageViewSharing();
  this.viewSharingSpiralOut.setSharingRatio(sharingRatio);
  this.viewSharingSpiralOut.setInput(this.pack3DSpiralOut.output());
  this.viewSharingSpiralOut.objectName = "View Sharing Spiral Out " + index;
  this.viewSharingSpiralOut.observeMultipleKeys(["acquisition.<Z Repeat>.index"]);
  this.viewSharingSpiralOut.observedKeysChanged.connect(setOutViewSharingChunk);

  function setOutViewSharingChunk(keys) {
    that.viewSharingSpiralOut.viewChunk = Math.floor(keys["acquisition.<Z Repeat>.index"] / zEncodesSharing);
  }

  this.zfftSpiralIn = new RthReconImageFFT();
  this.zfftSpiralIn.setTransformRank([false, false, true]);
  this.zfftSpiralIn.setInput(this.viewSharingSpiralIn.output());
  this.zfftSpiralIn.objectName = "zFFT Spiral In " + index;

  this.zfftSpiralOut = new RthReconImageFFT();
  this.zfftSpiralOut.setTransformRank([false, false, true]);
  this.zfftSpiralOut.setInput(this.viewSharingSpiralOut.output());
  this.zfftSpiralOut.objectName = "zFFT Spiral Out " + index;

  this.inoutCombiner = new RthReconImageAdd();
  this.inoutCombiner.objectName = "Spiral In Out Combine " + index;
  this.inoutCombiner.setInput(0, this.zfftSpiralIn.output());
  this.inoutCombiner.setInput(1, this.zfftSpiralOut.output());

  this.imageRouter = new RthReconImageRouteByIndex();
  this.imageRouter.setActiveOutput(0);
  this.imageRouter.observeValueForKey("reconstruction.imageType", "activeOutput");
  this.imageRouter.setInput(this.inoutCombiner.output());

  function resetPhaseRoute(geom) {
    that.phaseRouter.setActiveOutput(1);
    if (index == 0) {
      updateBasePhase(1);
    }
  }

  function setPhaseRoute(keys) {
    // 0 means to set base phase, 1 means to get the phase difference with the current base phase
    if (keys["reconstruction.basePhase"] == 0) {
      that.phaseRouter.setActiveOutput(1);
      if (index == 0) {
        updateBasePhase(1);
      }
    } else if (keys["reconstruction.basePhase"] == 1) {
      that.phaseRouter.setActiveOutput(0);
    }
  }

  this.phaseRouter = new RthReconImageRouteByIndex();
  this.phaseRouter.setActiveOutput(1);
  this.phaseRouter.observeMultipleKeys(["reconstruction.basePhase"]);
  this.phaseRouter.observedKeysChanged.connect(setPhaseRoute);
  this.phaseRouter.setInput(this.imageRouter.output(1));
  this.phaseRouter.geometryChanged.connect(resetPhaseRoute);

  this.conjugate = new RthReconImageConj();
  this.conjugate.setInput(this.phaseRouter.output(1));

  this.conjMultiply = new RthReconImageMultiply();
  this.conjMultiply.setInput(0, this.phaseRouter.output(0));
  this.conjMultiply.setInput(1, this.conjugate.output());
  this.conjMultiply.setPersistentInput(1);
  this.conjMultiply.informationPort = 0;

  this.phase = new RthReconImageArg();
  this.phase.objectName = "Phase " + index;
  this.phase.setInput(this.conjMultiply.output());

  this.phaseOutput = function() {
    return this.phase.output();
  }

  this.imageOutput = function() {
    return this.imageRouter.output(0);
  }
}

function reconSliceBlock(inputIn, inputOut, index) {  
  this.gridIn = new RthReconRawToImageGrid2D(spiralInGriddingOperator);
  this.gridIn.objectName = "Grid Spiral In " + index;
  this.gridIn.accumulate = interleaves;
  this.gridIn.setInput(inputIn);
  this.gridIn.geometryChanged.connect(this.gridIn.resetAccumulation);
    
  this.gridOut = new RthReconRawToImageGrid2D(spiralOutGriddingOperator);
  this.gridOut.objectName = "Grid Spiral Out " + index;
  this.gridOut.accumulate = interleaves;
  this.gridOut.setInput(inputOut);
  this.gridOut.geometryChanged.connect(this.gridOut.resetAccumulation);

  // FFT
  this.fftIn = new RthReconImageFFT();
  this.fftIn.objectName = "FFT Spiral In " + index;
  this.fftIn.setInput(this.gridIn.output());

  this.fftOut = new RthReconImageFFT();
  this.fftOut.objectName = "FFT Spiral Out " + index;
  this.fftOut.setInput(this.gridOut.output());
  
  // Deapodization by multiplying data with apodization window.
  this.deapodizeIn = new RthReconImageArrayMultiply();
  this.deapodizeIn.objectName = "Deapodize Spiral In " + index;
  this.deapodizeIn.setInput(0, spiralInDeapodizationWindow.output(-1));
  this.deapodizeIn.setInput(1, this.fftIn.output());
  this.deapodizeIn.setPersistentInput(0);
  this.deapodizeIn.informationPort = 1;

  this.deapodizeOut = new RthReconImageArrayMultiply();
  this.deapodizeOut.objectName = "Deapodize Spiral Out " + index;
  this.deapodizeOut.setInput(0, spiralOutDeapodizationWindow.output(-1));
  this.deapodizeOut.setInput(1, this.fftOut.output());
  this.deapodizeOut.setPersistentInput(0);
  this.deapodizeOut.informationPort = 1;
  
  this.spiralInOutput = function() {
    return this.deapodizeIn.output();
  }

  this.spiralOutOutput = function() {
    return this.deapodizeOut.output();
  }
}

// Final stage of coil composition by sum of squares.
var sumOfSquares = new RthReconImageSumOfSquares();
sumOfSquares.objectName = "SoS";

var phasePack = new RthReconImagePack();
phasePack.objectName = "Phase Pack";

var rxAtten = new RthReconRawApplyRxAttenuation();
rxAtten.objectName = "Rx Atten";
rxAtten.lowerLimit = 0.3;
rxAtten.upperLimit = 0.75;
// smooth attenuation changes over 1 second
rxAtten.memory = Math.ceil(1000.0/minTR);
rxAtten.newAttenuation.connect(function(newAtten) {
  rth.addCommand(new RthUpdateFloatParameterCommand(sequenceId, "sequence", "setRxAttenuation", "", newAtten));
});

// Intances of block must be stored in an array
// to avoid being garbage collected
var block = new Array();

function connectCoils(coils) {
  spiralInDeapodizationWindow.disconnectAllOutputs();
  spiralOutDeapodizationWindow.disconnectAllOutputs();
  block.length = 0;
  rth.collectGarbage();
  sumOfSquares.disconnectInputs();
  phasePack.disconnectInputs();
  rxAtten.disconnectInputs();
  for (var i = 0; i < coils ; i++) {
    block[i] = new reconBlock(observer.output(i), i);
    block[i].resetSlice();
    sumOfSquares.setInput(i, block[i].imageOutput());
    phasePack.setInput(i, block[i].phaseOutput());
    rxAtten.setInput(i, block[i].attenOutput());
  }
  spiralInGriddingOperator.recalculateOperator();
  spiralOutGriddingOperator.recalculateOperator();
}
// Call connectCoils if number of coils changed.
// This gets always called with the first acquisition.
observer.coilsChanged.connect(connectCoils);

var phaseMean = new RthReconImageAverage();
phaseMean.objectName = "Phase Mean";
phaseMean.setDirection(3);
phaseMean.setInput(phasePack.output());

var magUnwarp = new RthReconImageGradWarp();
magUnwarp.objectName = "Mag Unwarp";
magUnwarp.setKernelRadius(2);
magUnwarp.observeValueForKey("equipment.Signa/GradWarp/scalex3", "scaleX3");
magUnwarp.observeValueForKey("equipment.Signa/GradWarp/scalex5", "scaleX5");
magUnwarp.observeValueForKey("equipment.Signa/GradWarp/scaley3", "scaleY3");
magUnwarp.observeValueForKey("equipment.Signa/GradWarp/scaley5", "scaleY5");
magUnwarp.observeValueForKey("equipment.Signa/GradWarp/scalez3", "scaleZ3");
magUnwarp.observeValueForKey("equipment.Signa/GradWarp/scalez5", "scaleZ5");
magUnwarp.setInput(sumOfSquares.output());

var abs = new RthReconImageAbs();
abs.objectName = "Abs";
abs.setInput(magUnwarp.output());

var phaseUnwarp = new RthReconImageGradWarp();
phaseUnwarp.objectName = "Phase Unwarp";
phaseUnwarp.setKernelRadius(2);
phaseUnwarp.observeValueForKey("equipment.Signa/GradWarp/scalex3", "scaleX3");
phaseUnwarp.observeValueForKey("equipment.Signa/GradWarp/scalex5", "scaleX5");
phaseUnwarp.observeValueForKey("equipment.Signa/GradWarp/scaley3", "scaleY3");
phaseUnwarp.observeValueForKey("equipment.Signa/GradWarp/scaley5", "scaleY5");
phaseUnwarp.observeValueForKey("equipment.Signa/GradWarp/scalez3", "scaleZ3");
phaseUnwarp.observeValueForKey("equipment.Signa/GradWarp/scalez5", "scaleZ5");
phaseUnwarp.setInput(phaseMean.output());

var phaseToTemp = new RthReconImageScale();
phaseToTemp.objectName = "Phase to Temperature";
phaseToTemp.setInput(phaseUnwarp.output());
phaseToTemp.setScaling(scaling);

/*
var tempThreshold = new FUSReconImageThreshold();
tempThreshold.objectName = "Temporature Threshold";
tempThreshold.setInput(phaseToTemp.output());
*/

/*
var baseTempAdd = new FUSReconImageAddBaseline();
baseTempAdd.objectName = "Add Baseline Temp";
baseTempAdd.setInput(phaseToTemp.output());
*/

rth.importJS("lib:RthImageThreePlaneOutput.js");

var magThreePlane = new RthImageThreePlaneOutput();
magThreePlane.objectName = "Mag ThreePlane";
magThreePlane.setInput(abs.output());

var phaseThreePlane = new RthImageThreePlaneOutput();
phaseThreePlane.objectName = "Phase ThreePlane";
phaseThreePlane.setInput(phaseToTemp.output());

///// Setup GUI
function changeFlipAngle(angle) {
  if (angle > startingTip) angle = startingTip;
  var flipCommand = new RthUpdateFloatParameterCommand(sequenceId, "sequence", "scaleRF", "", angle/startingTip);
  rth.addCommand(flipCommand);
  rth.addCommand(new RthUpdateChangeMRIParameterCommand(sequenceId, "FlipAngle", angle));
}

function changeFOV(fov) {
  RTHLOGGER_INFO("change FOV called with fov = " + fov);
  fov *= 10;
  if (fov < startingFieldOfView) fov = startingFieldOfView;

  var scale = startingFieldOfView / fov;
  
  rth.addCommand(new RthUpdateChangeFieldOfViewCommand(sequenceId, fov, fov, zFieldOfView));
  rth.addCommand(new RthUpdateChangeResolutionCommand(sequenceId, resolution / scale));
  rth.addCommand(new RthUpdateScaleGradientsCommand(sequenceId, "sequence", scale, scale, startingZFieldOfView/zFieldOfView));

  navigation.setFOV(fov);
  navigation_ax.setFOV(fov);
  navigation_sa.setFOV(fov);
  navigation_co.setFOV(fov);

  fieldOfView = fov;
}

function changeSlabThickness(thickness) {
  RTHLOGGER_INFO("change slab thickness called with thickness = " + thickness);
  if (thickness < startingZFieldOfView) thickness = startingZFieldOfView;

  rth.addCommand(new RthUpdateScaleGradientsCommand(sequenceId, "sequence", startingFieldOfView/fieldOfView, startingFieldOfView/fieldOfView, startingZFieldOfView/thickness));
  rth.addCommand(new RthUpdateChangeSliceThicknessCommand(sequenceId, thickness/zEncodes));
  
  navigation.setSliceThickness(thickness);
  navigation_ax.setSliceThickness(thickness);
  navigation_sa.setSliceThickness(thickness);
  navigation_co.setSliceThickness(thickness);

  rth.addCommand(new RthUpdateFloatParameterCommand(sequenceId, "", "setSliceSeparation", "sliceSeparation", thickness/zEncodes));
  rth.addCommand(new RthUpdateChangeMRIParameterCommand(sequenceId, "SpacingBetweenSlices", thickness/zEncodes));
  rth.addCommand(new RthUpdateChangeFieldOfViewCommand(sequenceId, fieldOfView, fieldOfView, thickness));
  zFieldOfView = thickness;
}

function updateBasePhase(value) {
  rth.informationInsert(sequenceId, "reconstruction.basePhase", value);
}

function setImageType(imagetype) {
  rth.informationInsert(sequenceId, "reconstruction.imageType", imagetype);
}

function changeTempLowThreshold(low) {
  //tempThreshold.setLowThreshold(low);
}

function changeTempHighThreshold(high) {
  //tempThreshold.setHighThreshold(high);
}

function changeViewSharing(ratio) {
  sharingRatio = ratio;
  zEncodesSharing = zEncodes / sharingRatio;
}

controlWidget.inputWidget_FOV.valueChanged.connect(changeFOV);
controlWidget.inputWidget_FlipAngle.valueChanged.connect(changeFlipAngle);
controlWidget.inputWidget_SlabThickness.valueChanged.connect(changeSlabThickness);
controlWidget.inputWidget_TempLowThreshold.valueChanged.connect(changeTempLowThreshold);
controlWidget.inputWidget_TempHighThreshold.valueChanged.connect(changeTempHighThreshold);
controlWidget.inputWidget_ViewSharing.valueChanged.connect(changeViewSharing);

// This method overrides the default enable scan.
rth.enableScan = function(state) 
{
  if (state) {
    updateBasePhase(0);
    adjustFramePosition(0);
    spiralInDeapodizationWindow.disconnectAllOutputs();
    spiralOutDeapodizationWindow.disconnectAllOutputs();
    for (var m = 0; m < block.length; m++) {
      block[m].resetSlice();
    }
    spiralInGriddingOperator.recalculateOperator();
    spiralOutGriddingOperator.recalculateOperator();
  }
  var enableCommand = RthUpdateEnableSequenceCommand(sequenceId, state);
  rth.addCommand(enableCommand);
}

rth.updateSharedParameter = function(key, value) {
  if(key == "xGradientDelay") {
    rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "xGradientDelay", value));
  } else if(key=="yGradientDelay") {
    rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "yGradientDelay", value));
  } else if(key=="zGradientDelay") {
    rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "zGradientDelay", value));
  } else if(key == "shims") {
    rth.addCommand(new RthUpdateShimsCommand(sequenceId, value[0], value[1], value[2], value[3]));
  }
}

// Setup GUI defaults and limits
rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "xGradientDelay", 0));
rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "yGradientDelay", 0));
rth.addCommand(new RthUpdateChangeReconstructionParameterCommand(sequenceId, "zGradientDelay", 0));

// Setup GUI defaults and limits
controlWidget.inputWidget_FOV.minimum = startingFieldOfView / 10;
controlWidget.inputWidget_FOV.maximum = 48;
controlWidget.inputWidget_FOV.value = startingFieldOfView / 10;
changeFOV(controlWidget.inputWidget_FOV.value);

controlWidget.inputWidget_FlipAngle.minimum = 0;
controlWidget.inputWidget_FlipAngle.maximum = startingTip;
controlWidget.inputWidget_FlipAngle.value = startingTip;
changeFlipAngle(controlWidget.inputWidget_FlipAngle.value);

controlWidget.inputWidget_SlabThickness.minimum = startingZFieldOfView;
controlWidget.inputWidget_SlabThickness.maximum = startingZFieldOfView * 4;
controlWidget.inputWidget_SlabThickness.value = startingZFieldOfView;
changeSlabThickness(controlWidget.inputWidget_SlabThickness.value);

changeTempLowThreshold(controlWidget.inputWidget_TempLowThreshold.value);
changeTempHighThreshold(controlWidget.inputWidget_TempHighThreshold.value);

controlWidget.inputWidget_ViewSharing.minimum = 1;
controlWidget.inputWidget_ViewSharing.maximum = 4;
controlWidget.inputWidget_ViewSharing.value = 1;
changeViewSharing(controlWidget.inputWidget_ViewSharing.value);

function changeToMag() {
  setImageType(0);
}

function changeToPhase() {
  setImageType(1);
}

controlWidget.radioButton_Mag.clicked.connect(changeToMag);
controlWidget.radioButton_Phase.clicked.connect(changeToPhase);
