/***************************************************************************
 
 Copyright 2014 HeartVista Inc.  All rights reserved.
 Contact: HeartVista, Inc. <rthawk-info@heartvista.com>
 
 This file is part of the RTHawk system.
 
 $HEARTVISTA_BEGIN_LICENSE$
 
 THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF HEARTVISTA
 The copyright notice above does not evidence any	
 actual or intended publication of such source code.
 
 $HEARTVISTA_END_LICENSE$
 
 ***************************************************************************/
#include "ellipsemask.h"
#include "RthReconData.h"

#include <complex.h>

/*! \ingroup Reconstruction */
/*\@{*/

/*! \class RthReconCustomFilter
 \brief Sample reconstruction filter that applies a Fermi Filter to the input
 
 This filter is an example on how to create a custom reconstruction filter. The functionality
 is similar to the Fermi filter node.

 This filter accepts a single input and scales it by the following expression:
 
 \f[
 \frac{1}{(1 + e^{((d - R) / W))}}
 \f]
 
 Where \f$d\f$ is the pixel distance from the center of the image, \f$R\f$ is the filter radius and \f$W\f$ is the transition width. All expressed as a proportion of the FOV.
 
 By default the filter is calculated as a separable function but it can configured to be non-separable. In addition, the filter can be streched and off-centered.  Off-centering is only supported when the filter is used in the spatial domain, and the center location is input in mm.
 */ 

RTHLOGGER_CREATE_PREFIX("Custom", ellipsemask)

ellipsemask::ellipsemask(QObject* parent) :
  RthReconFilter(parent),
  m_rotangle(0),
  m_xoffset(0),
  m_yoffset(0),
  m_xdiameter(100),
  m_ydiameter(100),
  m_rebuildmask(true),
  m_verbose(false)
{
}

ellipsemask::~ellipsemask()
{
  cleanup();
}

void ellipsemask::execute(QList<const RthReconData*> input, QList<RthReconData*> output)
{

  int i,j;
  float ell;
  float c,s;
  float xrad2,yrad2;
  float xoff,yoff;

  if (input[0]->dimensions() < 2) {
    RTHLOGGER_ERROR(objectName() << "Ellipse mask only supports 2D inputs");
  }

  if(m_verbose){
    RTHLOGGER_INFO(objectName() << "In ellipse mask execute() function");
  }

  output[0]->newData(input[0]->information(), input[0]->extent());

  const complexFloat *in = input[0]->constData();
  complexFloat *out = output[0]->data();

  int nx = input[0]->extent(0); // matrix size, number of readout points
  int ny = input[0]->extent(1); // matrix size, number of phase encodes

  if (m_nx != nx || m_ny != ny){
    m_nx = nx;
    m_ny = ny;
    m_rebuildmask = true;
  }

  if(m_rebuildmask){
    
    // resize the vectors
    m_mask.resize(m_nx*m_ny);

    xrad2 = pow(m_xdiameter/200.0*(float)m_nx,2.0);
    yrad2 = pow(m_ydiameter/200.0*(float)m_ny,2.0);

    xoff = m_xoffset/100.0*(float)m_nx;
    yoff = m_yoffset/100.0*(float)m_ny;

    c = cos(m_rotangle*PI/180.0);
    s = sin(m_rotangle*PI/180.0);

    if(xrad2 > 0 && yrad2 > 0){

      for(i = 0;i < m_nx;i++){
	for(j = 0;j < m_ny;j++){
	  
	  ell = pow(((float)i - (float)m_nx/2.0 - xoff)*c + 
		    ((float)j - (float)m_ny/2.0 - yoff)*s,2.0)/xrad2 + 
	    pow(((float)i - (float)m_nx/2.0 - xoff)*-s + 
		((float)j - (float)m_ny/2.0 - yoff)*c,2.0)/yrad2;
	  
	  m_mask[j*m_nx+i].real(ell <= 1.0 ? 1.0 : 0.0);
	  
	}
      }

    } else {

      for(i = 0;i < m_nx*m_ny;i++) m_mask[i] = 0.0;
      
    }

    // don't recalc
    m_rebuildmask = false;

    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "Recalculated ellipse mask.");
    }    

  }
  
  for(i = 0;i < m_nx*m_ny;i++) out[i] = in[i]*m_mask[i];

  if(m_verbose){
    RTHLOGGER_INFO(objectName() << "Applied ellipse mask. Exiting execute().");
  }

}

void ellipsemask::cleanup() 
{

  m_mask.clear();
  m_rebuildmask = true;

}

float ellipsemask::rotangle()
{
  return m_rotangle;
}

void ellipsemask::setRotangle(float rotangle)
{
  m_rotangle = rotangle;
  m_rebuildmask = true;
}

float ellipsemask::xoffset()
{
  return m_xoffset;
}

void ellipsemask::setXoffset(float xoffset)
{
  m_xoffset = xoffset;
  m_rebuildmask = true;
}

float ellipsemask::yoffset()
{
  return m_yoffset;
}

void ellipsemask::setYoffset(float yoffset)
{
  m_yoffset = yoffset;
  m_rebuildmask = true;
}

float ellipsemask::xdiameter()
{
  return m_xdiameter;
}

void ellipsemask::setXdiameter(float xdiameter)
{
  m_xdiameter = xdiameter;
  m_rebuildmask = true;
}

float ellipsemask::ydiameter()
{
  return m_ydiameter;
}

void ellipsemask::setYdiameter(float ydiameter)
{
  m_ydiameter = ydiameter;
  m_rebuildmask = true;
}

bool ellipsemask::verbose()
{
  return m_verbose;
}

void ellipsemask::setVerbose(bool verbose)
{
  m_verbose = verbose;
}
