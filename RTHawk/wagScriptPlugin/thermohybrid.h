/***************************************************************************
 
 Copyright 2014 HeartVista Inc.  All rights reserved.
 Contact: HeartVista, Inc. <rthawk-info@heartvista.com>
 
 This file is part of the RTHawk system.
 
 $HEARTVISTA_BEGIN_LICENSE$
 
 THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF HEARTVISTA
 The copyright notice above does not evidence any	
 actual or intended publication of such source code.
 
 $HEARTVISTA_END_LICENSE$
 
 ***************************************************************************/
#ifndef THERMOHYBRID_H
#define THERMOHYBRID_H

#include "RthReconFilter.h"

#include <QtScript>

#include "blas.h"

#define PI (3.141592653589793)
#define TWOPI (6.28318530718)

class thermohybrid : public RthReconFilter {
  Q_OBJECT
  Q_CLASSINFO("maxInputs","2")
  Q_CLASSINFO("minInputs","2")
  Q_CLASSINFO("maxOutputs","1")
  Q_CLASSINFO("minOutputs","1")
  Q_PROPERTY(int order READ order WRITE setOrder)
  Q_PROPERTY(float lam READ lam WRITE setLam)
  Q_PROPERTY(float maskthresh READ maskthresh WRITE setMaskthresh)
  Q_PROPERTY(float stopthresh READ stopthresh WRITE setStopthresh)
  Q_PROPERTY(bool verbose READ verbose WRITE setVerbose)
  RTH_UNIT_TEST(thermohybrid)
  RTHLOGGER_DECLARE
public:
  thermohybrid(QObject* parent = 0);
  ~thermohybrid();
public slots:
  int order();
  void setOrder(int order);
  float lam();
  void setLam(float lam);
  float maskthresh();
  void setMaskthresh(float maskthresh);
  float stopthresh();
  void setStopthresh(float stopthresh);
  bool verbose();
  void setVerbose(bool verbose);
protected:
  void execute(QList<const RthReconData*> input, QList<RthReconData*> output);
  void cleanup();
private:
private:
  int m_order;
  float m_lam;
  float m_maskthresh;
  float m_stopthresh;
  int m_nx;
  int m_ny;
  int m_np;
  bool m_rebuildvectors;
  bool m_verbose;
  QVector<float> m_m;
  QVector<float> m_dynmag;
  QVector<float> m_dynphs;
  QVector<float> m_basemag;
  QVector<float> m_basephs;
  QVector<float> m_Ac;
  QVector<float> m_A;
  QVector<float> m_c;
  QVector<bool> m_mask;
};

float cost_eval(float *dynmag,float *dynphs,float *Ac,float *m,float *basemag,
		float *basephs,float lam,int n);

void m_update(float *dynmag,float *dynphs,float *Ac,float *m,float *basemag,
	      float *basephs,float lam,int n,bool *mask);// int nc);

void c_update(float *dynmag,float *dynphs, 
	      float *c,float *Ac,
	      float *m,float *basemag,float *basephs,int n);

RTHRECON_DECLARE_QMETAOBJECT(thermohybrid, QObject*)
#endif
