/***************************************************************************
 
 Copyright 2014 HeartVista Inc.  All rights reserved.
 Contact: HeartVista, Inc. <rthawk-info@heartvista.com>
 
 This file is part of the RTHawk system.
 
 $HEARTVISTA_BEGIN_LICENSE$
 
 THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF HEARTVISTA
 The copyright notice above does not evidence any	
 actual or intended publication of such source code.
 
 $HEARTVISTA_END_LICENSE$
 
 ***************************************************************************/
#include "thermocscorr.h"
#include "RthReconData.h"

#include <complex.h>

/*! \ingroup Reconstruction */
/*\@{*/

/*! \class RthReconCustomFilter
 \brief Sample reconstruction filter that applies a Fermi Filter to the input
 
 This filter is an example on how to create a custom reconstruction filter. The functionality
 is similar to the Fermi filter node.

 This filter accepts a single input and scales it by the following expression:
 
 \f[
 \frac{1}{(1 + e^{((d - R) / W))}}
 \f]
 
 Where \f$d\f$ is the pixel distance from the center of the image, \f$R\f$ is the filter radius and \f$W\f$ is the transition width. All expressed as a proportion of the FOV.
 
 By default the filter is calculated as a separable function but it can configured to be non-separable. In addition, the filter can be streched and off-centered.  Off-centering is only supported when the filter is used in the spatial domain, and the center location is input in mm.
 */ 

RTHLOGGER_CREATE_PREFIX("Custom", thermocscorr)

thermocscorr::thermocscorr(QObject* parent) :
  RthReconFilter(parent),
  m_stepsize(0.0001),
  m_maskthresh(-0.1),
  m_stopthresh(0.999),
  m_tro(1),
  m_te(10),
  m_driftcomp(false),
  m_nro(0),
  m_npe(0),
  m_maxcorrvoxels(500),
  m_improdthresh(0.1),
  m_rebuildvectors(true),
  m_passthrough(true),
  m_verbose(false)
{
}

thermocscorr::~thermocscorr()
{
  cleanup();
}

void thermocscorr::execute(QList<const RthReconData*> input, QList<RthReconData*> output)
{
  if (input[0]->dimensions() < 2) {
    RTHLOGGER_ERROR(objectName() << "CS Correction only supports 2D inputs");
  }

  //RTHLOGGER_INFO(objectName() << "In CS Correction execute() function");

  //rth.information.te.?
  output[0]->newData(input[2]->information(), input[2]->extent());

  const complexFloat *f = input[0]->constData();
  const complexFloat *imghot = input[1]->constData();
  const complexFloat *m = input[2]->constData();
  complexFloat *mout = output[0]->data();

  m_te = (float)input[1]->information().mri().echoTime(); // ms
  //RTHLOGGER_INFO(objectName() << "CS Corr tro = " << m_tro);

  int i,j,k,nx,niter;
  int nro = input[0]->extent(0); // matrix size, number of readout points
  int npe = input[0]->extent(1); // matrix size, number of phase encodes

  if (m_nro != nro || m_npe != npe){
    m_nro = nro;
    m_npe = npe;
    m_rebuildvectors = true;
  }

  if(m_rebuildvectors){
    m_tte.resize(m_nro);
    m_mask.resize(m_nro);
    m_xm.resize(m_nro);
    m_baseimgr.resize(m_nro);
    m_baseimgi.resize(m_nro);
    m_datar.resize(m_nro);
    m_datai.resize(m_nro);
#ifdef USEKISSFFT
    m_ftbuffer.resize(m_nro);
    m_ftbufferout.resize(m_nro);
    m_fftcfg = kiss_fft_alloc(m_nro,0,NULL,NULL);
#else
    m_ftbuffer.resize(2*m_nro);
#endif    
    m_mrm.resize(m_nro);
    m_mim.resize(m_nro);
    m_auxr.resize(m_nro);
    m_auxi.resize(m_nro);
    m_Ar.resize(m_nro*m_nro);
    m_Ai.resize(m_nro*m_nro);
    m_gr.resize(m_nro);
    m_gi.resize(m_nro);
    m_rebuildvectors = false;
    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "Rebuilt CS Corr vectors. m_nro = " << m_nro << ". m_npe = " << m_npe);
    }
  }

  float cc,sc;
  float c = 0; // REDFLAG: how to get/set in real time?
  float err,olderr;

  char chn = 'N'; /* not transposed */
  double one = 1.0, zero = 0.0, two = 2.0, mintwo = -2.0;
  int onei = 1;

  int totalvoxels = 0;
  float impt,tmp;

  if(!m_passthrough){
    
    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "In CS Correction mode");    
    }

    for(i = 0;i < m_nro;i++)
      m_tte[i] = (-m_nro/2+i)/(float)(m_nro)*(m_tro/m_te)+1.0;

    for(k = 0;k < m_npe;k++){ // loop over columns
      
      // get magnitude threshold for this column
      impt = 0;
      for(i = 0;i < m_nro;i++){
	tmp = abs(f[k*m_nro+i])*abs(imghot[k*m_nro+i]);
	if(impt < tmp) impt = tmp;
      }
      impt *= m_improdthresh;

      // get mask for this column
      nx = 0;
      for(i = 0;i < m_nro;i++){
	m_mask[i] = (m[k*m_nro+i].real() < m_maskthresh) && (abs(f[k*m_nro+i])*abs(imghot[k*m_nro+i]) > impt);
	nx += (int)m_mask[i];
      }
      
      totalvoxels += nx;
      
      if(m_verbose){
	RTHLOGGER_INFO(objectName() << "In column " << k << ". Found nx = " << nx << " nonzeros.");
      }

      // if there is significant heat in this column...
      if((nx > 0) && (totalvoxels < m_maxcorrvoxels)){
	
	/* copy the mask */
	j = 0;
	for(i = 0;i < m_nro;i++)
	  if(m_mask[i])
	    m_xm[j++] = (float)(-m_nro/2+i)/(float)(m_nro);

	//RTHLOGGER_INFO(objectName() << "Calculated masked spatial locations");
	
	/* calculate the baseline image in the mask */
	j = 0;
	cc = cos(c);sc = sin(c);
	for(i = 0;i < m_nro;i++)
	  if(m_mask[i]){
	    if(m_driftcomp){
	      m_baseimgr[j] = f[k*m_nro+i].real()*cc - f[k*m_nro+i].imag()*sc;
	      m_baseimgi[j] = f[k*m_nro+i].imag()*cc + f[k*m_nro+i].real()*sc;
	    } else {
	      m_baseimgr[j] = f[k*m_nro+i].real();
	      m_baseimgi[j] = f[k*m_nro+i].imag();
	    }
	    j++;
	  }

	//RTHLOGGER_INFO(objectName() << "Got baseline in the mask");
	
	/* data1dft = imghot(:,ii) - ~mask(:,ii).*imgc.*exp(1i*mhybr(:,ii)); */
	for(i = 0;i < m_nro;i++){
	  m_datar[i] = imghot[k*m_nro+i].real();
	  m_datai[i] = imghot[k*m_nro+i].imag();
	  if(!m_mask[i]){
	    if(m_driftcomp){
	      cc = cos(m[k*m_nro+i].real()+c);
	      sc = sin(m[k*m_nro+i].real()+c);
	    }
	    else{
	      cc = cos(m[k*m_nro+i].real());
	      sc = sin(m[k*m_nro+i].real());
	    }
	    m_datar[i] -= f[k*m_nro+i].real()*cc - f[k*m_nro+i].imag()*sc;
	    m_datai[i] -= f[k*m_nro+i].imag()*cc + f[k*m_nro+i].real()*sc;
	  }
	}
	
	//RTHLOGGER_INFO(objectName() << "Got FT data");

	/* data1dft = ft(data1dft); */
#ifndef USEKISSFFT
	ft(m_ftbuffer.data(),m_datar.data(),m_datai.data(),m_nro);
#else
	for(i = 0;i < m_nro;i++){
	  m_ftbuffer[i].r = m_datar[(i+m_nro/2)%m_nro];
	  m_ftbuffer[i].i = m_datai[(i+m_nro/2)%m_nro];
	}
	kiss_fft( m_fftcfg , m_ftbuffer.data() , m_ftbufferout.data() );
	for(i = 0;i < m_nro;i++){
	  m_datar[(i+m_nro/2)%m_nro] = m_ftbufferout[i].r;
	  m_datai[(i+m_nro/2)%m_nro] = m_ftbufferout[i].i;
	}
#endif
	
	//RTHLOGGER_INFO(objectName() << "Completed FT");

	/* copy m -> m_masked, in masked locations */
	j = 0;
	for(i = 0;i < m_nro;i++)
	  if(m_mask[i]){
	    m_mrm[j] = m[k*m_nro+i].real();
	    m_mim[j] = 0;
	    j++;
	  }
	
	//RTHLOGGER_INFO(objectName() << "Got mrm");

	olderr = -1;
	err = -1;
	niter = 0;
	while((err < m_stopthresh*olderr) || (err == -1) || (olderr == -1)){
	  
	  //RTHLOGGER_INFO(objectName() << "In loop");

	  /* A = exp(-1i*2*pi*(-dim/2:dim/2-1)'*x+1i*(tte*real(m(:)).')); */
	  /* A = bsxfun(@times,A,exp(-imag(m(:))).'.*baseimg(:).'); */
	  /* aux = data-sum(A,2); */
	  for(i = 0;i < m_nro;i++){
	    m_auxr[i] = m_datar[i];
	    m_auxi[i] = m_datai[i];
	  }
	  //RTHLOGGER_INFO(objectName() << "Got aux");
	  buildA(m_Ar.data(), m_Ai.data(), m_xm.data(), m_tte.data(), m_mrm.data(), m_mim.data(), m_baseimgr.data(), m_baseimgi.data(), m_auxr.data(), m_auxi.data(), m_nro, nx);

	  //RTHLOGGER_INFO(objectName() << "Built A");
	  
	  /* err = norm(aux) */
	  olderr = err;
	  err = norm2c(m_nro,m_auxr.data(),m_auxi.data());
	  if(m_verbose){
	    RTHLOGGER_INFO(objectName() << "col k = " << k << ". err = " << err);    
	  }
	  
	  //RTHLOGGER_INFO(objectName() << "Calculated error");

	  /* gi = real(2*(A'*aux)); */
	  dgemv_(&chn, &nx, &m_nro, &one, m_Ar.data(), &nx, m_auxr.data(), &onei, &zero, m_gi.data(), &onei);
	  dgemv_(&chn, &nx, &m_nro, &two, m_Ai.data(), &nx, m_auxi.data(), &onei, &two, m_gi.data(), &onei);
	  /* gr = -imag(2*(A'*(tte.*aux))); */
	  for(i = 0;i < m_nro;i++){
	    m_auxr[i] *= m_tte[i];
	    m_auxi[i] *= m_tte[i];
	  }
	  dgemv_(&chn, &nx, &m_nro, &one, m_Ar.data(), &nx, m_auxi.data(), &onei, &zero, m_gr.data(), &onei);
	  dgemv_(&chn, &nx, &m_nro, &two, m_Ai.data(), &nx, m_auxr.data(), &onei, &mintwo, m_gr.data(), &onei);
	  
	  //RTHLOGGER_INFO(objectName() << "Multiplied matrices");

	  for(i = 0;i < nx;i++){
	    
	    /* step in negative deriv direction */
	    m_mrm[i] -= m_stepsize*m_gr[i];
	    m_mim[i] -= m_stepsize*m_gi[i];
	    
	    /* enforce negative real part, positive imag part */
	    m_mrm[i] = m_mrm[i] < 0 ? m_mrm[i] : 0;
	    m_mim[i] = m_mim[i] > 0 ? m_mim[i] : 0;
	    
	  }		
	  
	  //RTHLOGGER_INFO(objectName() << "Updated m");
	  niter++;
	} // while loop
	
	if(m_verbose){
	  RTHLOGGER_INFO(objectName() << niter << "iterations");
	}

      } // nx > 0
      //else
      //	{
	  //if(totalvoxels > m_maxcorrvoxels)
	    //RTHLOGGER_INFO(objectName() << "Skipping column k = " << k << ", past maxcorrvoxels.");
      //	}
      
      /* copy masked result to full vector */
      j = 0;
      for(i = 0;i < m_nro;i++){
	if(!m_mask[i]){
	  mout[k*m_nro+i].real(m[k*m_nro+i].real());
	} else {
	  mout[k*m_nro+i].real(m_mrm[j]);
	  //mout[k*nro+i].imag(m_mim[j]);
	  j++;
	}
      }    
      //RTHLOGGER_INFO(objectName() << "Copied result to output. Done with column k = " << k);
      
    } // for(k = 0;k < npe;k++)

  } else { // copy input temp map -> output
    
    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "In CS Correction passthrough mode");    
    }

    for(i = 0;i < m_nro*m_npe;i++) mout[i].real(m[i].real());

  }


}

void thermocscorr::cleanup() 
{

  m_tte.clear();
  m_mask.clear();
  m_xm.clear();
  m_baseimgr.clear();
  m_baseimgi.clear();
  m_datar.clear();
  m_datai.clear();
#ifdef USEKISSFFT
  m_ftbuffer.clear();
  m_ftbufferout.clear();
  if(!m_rebuildvectors) free(m_fftcfg);
#else
  m_ftbuffer.clear();
#endif
  m_mrm.clear();
  m_mim.clear();
  m_auxr.clear();
  m_auxi.clear();
  m_Ar.clear();
  m_Ai.clear();
  m_gr.clear();
  m_gi.clear();
  m_rebuildvectors = true;

}

float thermocscorr::stepsize()
{
  return m_stepsize;
}

void thermocscorr::setStepsize(float stepsize)
{
  m_stepsize = stepsize;
}

float thermocscorr::maskthresh()
{
  return m_maskthresh;
}

void thermocscorr::setMaskthresh(float maskthresh)
{
  m_maskthresh = maskthresh;
}

float thermocscorr::stopthresh()
{
  return m_stopthresh;
}

void thermocscorr::setStopthresh(float stopthresh)
{
  m_stopthresh = stopthresh;
}

float thermocscorr::tro()
{
  return m_tro;
}

void thermocscorr::settro(float tro)
{
  m_tro = tro;
}

float thermocscorr::te()
{
  return m_te;
}

void thermocscorr::sette(float te)
{
  m_te = te;
}

float thermocscorr::improdthresh()
{
  return m_improdthresh;
}

void thermocscorr::setImprodthresh(float improdthresh)
{
  m_improdthresh = improdthresh;
}

int thermocscorr::maxcorrvoxels()
{
  return m_maxcorrvoxels;
}

void thermocscorr::setMaxcorrvoxels(int maxcorrvoxels)
{
  m_maxcorrvoxels = maxcorrvoxels;
}

bool thermocscorr::driftcomp()
{
  return m_driftcomp;
}

void thermocscorr::setDriftcomp(bool driftcomp)
{
  m_driftcomp = driftcomp;
}

bool thermocscorr::passthrough()
{
  return m_passthrough;
}

void thermocscorr::setPassthrough(bool passthrough)
{
  m_passthrough = passthrough;
}

bool thermocscorr::verbose()
{
  return m_verbose;
}

void thermocscorr::setVerbose(bool verbose)
{
  m_verbose = verbose;
}

#ifndef USEKISSFFT
/* Call four1 FFT with fftshift and separate real/imag input/output */
void ft(float *ftbuffer, float *datar, float *datai, long n){
    
    int i;
    /* put data into fourier-transformable (real/imag-interleaved) format */
    for(i = 0;i < n;i++){
        ftbuffer[2*i] = datar[(i+n/2)%n];
        ftbuffer[2*i+1] = datai[(i+n/2)%n];
    }
    /* calculate data FT */
    four1(ftbuffer-1,n,-1);
    /* put FT'd data back into data vectors */
    for(i = 0;i < n;i++){
        datar[(i+n/2)%n] = ftbuffer[2*i];
        datai[(i+n/2)%n] = ftbuffer[2*i+1];
    }
    
}
#endif

/* l2 norm of a complex vector */
float norm2c(int n,double* inr,double* ini){
    int i;
    float out = 0;
    for(i = 0;i < n;i++){
        out += inr[i]*inr[i] + ini[i]*ini[i];
    }
    return sqrt(out);
}

/* build system matrix for CS compensation */
 void buildA(double *Ar, double *Ai, float *x, float *tte,
	     float *mr, float *mi, float *baseimgr, float *baseimgi,
	     double *auxr, double *auxi, int nro, int nx){

    int kmin = -nro/2;
    int i,j;
    float arg;
    float tmpr,tmpi;
    for(j = 0;j < nro;j++){ /* col counter (k-space) */
        
        for(i = 0;i < nx;i++){ /* row counter (space) */
            
            arg = -2*PI*(kmin + j)*x[i] + tte[j]*mr[i];
            
            tmpr = exp(-mi[i])*cos(arg);
            tmpi = exp(-mi[i])*sin(arg);
            
            Ar[j*nx+i] = tmpr*baseimgr[i] - tmpi*baseimgi[i];
            Ai[j*nx+i] = tmpi*baseimgr[i] + tmpr*baseimgi[i];
            auxr[j] -= Ar[j*nx+i];
            auxi[j] -= Ai[j*nx+i];
            
        }
    }
    
}
