/***************************************************************************
 
 Copyright 2014 HeartVista Inc.  All rights reserved.
 Contact: HeartVista, Inc. <rthawk-info@heartvista.com>
 
 This file is part of the RTHawk system.
 
 $HEARTVISTA_BEGIN_LICENSE$
 
 THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF HEARTVISTA
 The copyright notice above does not evidence any	
 actual or intended publication of such source code.
 
 $HEARTVISTA_END_LICENSE$
 
 ***************************************************************************/
#include "thermohybrid.h"
#include "RthReconData.h"

#include <complex.h>

/*! \ingroup Reconstruction */
/*\@{*/

/*! \class RthReconCustomFilter
 \brief Sample reconstruction filter that applies a Fermi Filter to the input
 
 This filter is an example on how to create a custom reconstruction filter. The functionality
 is similar to the Fermi filter node.

 This filter accepts a single input and scales it by the following expression:
 
 \f[
 \frac{1}{(1 + e^{((d - R) / W))}}
 \f]
 
 Where \f$d\f$ is the pixel distance from the center of the image, \f$R\f$ is the filter radius and \f$W\f$ is the transition width. All expressed as a proportion of the FOV.
 
 By default the filter is calculated as a separable function but it can configured to be non-separable. In addition, the filter can be streched and off-centered.  Off-centering is only supported when the filter is used in the spatial domain, and the center location is input in mm.
 */ 

RTHLOGGER_CREATE_PREFIX("Custom", thermohybrid)

thermohybrid::thermohybrid(QObject* parent) :
  RthReconFilter(parent),
  m_order(0),
  m_lam(0.01),
  m_maskthresh(0.1),
  m_stopthresh(0.999),
  m_nx(0),
  m_ny(0),
  m_rebuildvectors(true),
  m_verbose(false)
{
}

thermohybrid::~thermohybrid()
{
  cleanup();
}

void thermohybrid::execute(QList<const RthReconData*> input, QList<RthReconData*> output)
{
  if (input[0]->dimensions() < 2) {
    RTHLOGGER_ERROR(objectName() << "Thermo hyrbid only supports 2D inputs");
  }

  if(m_verbose){
    RTHLOGGER_INFO(objectName() << "In thermo hybrid execute() function");
  }

  output[0]->newData(input[0]->information(), input[0]->extent());

  const complexFloat *f = input[0]->constData();
  const complexFloat *imghot = input[1]->constData();
  complexFloat *mout = output[0]->data();

  int i,j,k,l,n;
  int nx = input[0]->extent(0); // matrix size, number of readout points
  int ny = input[0]->extent(1); // matrix size, number of phase encodes

  if (m_nx != nx || m_ny != ny){
    m_nx = nx;
    m_ny = ny;
    m_rebuildvectors = true;
  }

  n = m_nx*m_ny;

  if(m_rebuildvectors){
    
    // calculate number of poly coeffs
    m_np = 0;
    for(i = 1;i <= m_order+1;i++) m_np++;

    // resize the vectors
    m_m.resize(n);
    m_dynmag.resize(n);
    m_dynphs.resize(n);
    m_basemag.resize(n);
    m_basephs.resize(n);
    m_Ac.resize(n);
    m_A.resize(n*m_np);
    m_c.resize(m_np);
    m_mask.resize(n);

    // rebuild polynomial matrix
    int ctr = 0;
    for(i = 0;i <= m_order;i++)
      for(j = 0;j <= m_order-i;j++){
	for(k = 0;k < m_nx;k++)
	  for(l = 0;l < m_ny;l++)
	    m_A[ctr*n+l*m_nx+k] = pow(-1.0+2.0/(float)(m_nx-1)*(float)k,j)*
	      pow(-1.0+2.0/(float)(m_ny-1)*(float)l,i);
	ctr++;
      }

    // don't do it again next frame
    m_rebuildvectors = false;
    
    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "Rebuilt thermo hybrid vectors. m_nx = " << m_nx << ". m_ny = " << m_nx << ". m_np = " << m_np);
    }

  }
  
  // init temp phase map
  for(i = 0;i < n;i++) m_m[i] = 0.0;

  /* initialize poly phase shift */
  for(i = 0;i < n;i++) m_Ac[i] = 0.0;

  /* initialize coefficients */
  for(i = 0;i < m_np;i++) m_c[i] = 0;
  
  // initialize mask
  for(i = 0;i < n;i++) m_mask[i] = true;

  /* get magnitude and phase of baseline and dynamic images */
  for(i = 0;i < n;i++){
    m_dynmag[i] = sqrt(imghot[i].real()*imghot[i].real()+imghot[i].imag()*imghot[i].imag());
    m_dynphs[i] = atan2(imghot[i].imag(),imghot[i].real());
    m_basemag[i] = sqrt(f[i].real()*f[i].real()+f[i].imag()*f[i].imag());
    m_basephs[i] = atan2(f[i].imag(),f[i].real());
  }

  if(m_verbose){
    RTHLOGGER_INFO(objectName() << "all vars initialized.");
  }

  float cost,costold;
  //int onei = 1;

  cost = -1;
  costold = -1;

  while((cost < m_stopthresh*costold) | (cost == -1) | (costold == -1)){

    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "updating m.");
    }
    // update temp phase shift
    m_update(m_dynmag.data(),m_dynphs.data(),m_Ac.data(),
	     m_m.data(),m_basemag.data(),m_basephs.data(),
	     m_lam,n,m_mask.data());
    
    // update poly phase shift coeffs
    c_update(m_dynmag.data(),m_dynphs.data(),m_c.data(),
	     m_Ac.data(),m_m.data(),m_basemag.data(),m_basephs.data(),n);
    // recalculate spatial poly phase shift
    //dgemv_(&chn, &n, &np, &one, A, &n, c, &onei, &zero, Ac, &onei);
    for(i = 0;i < n;i++) m_Ac[i] = m_c[0];
    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "c[0] = " << m_c[0] << ".");
    }
    
    costold = cost;
    cost = cost_eval(m_dynmag.data(),m_dynphs.data(),m_Ac.data(),
		     m_m.data(),m_basemag.data(),m_basephs.data(),
		     m_lam,n);
    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "cost = " << cost << ".");
    }

  }

  // update mask to significant heating voxels
  int nmask = 0;
  for(i = 0;i < n;i++){
    m_mask[i] = m_m[i] > m_maskthresh;
    if(!m_mask[i]) m_m[i] = 0;
    nmask += (int)m_mask[i];
  }

  if(m_verbose){
    RTHLOGGER_INFO(objectName() << "calculated mask.");
  }

  // masked iterations
  cost = -1;
  costold = -1;
  while((nmask > 0) && ((cost < m_stopthresh*costold) | (cost == -1) | (costold == -1))){

    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "updating m.");
    }
    // update temp phase shift
    m_update(m_dynmag.data(),m_dynphs.data(),m_Ac.data(),m_m.data(),
	     m_basemag.data(),m_basephs.data(),m_lam,n,
	     m_mask.data());

    // update poly phase shift coeffs
    c_update(m_dynmag.data(),m_dynphs.data(),m_c.data(),m_Ac.data(),
	     m_m.data(),m_basemag.data(),m_basephs.data(),n);
    // recalculate spatial poly phase shift
    //dgemv_(&chn, &n, &np, &one, A, &n, c, &onei, &zero, Ac, &onei);
    for(i = 0;i < n;i++) m_Ac[i] = m_c[0];
    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "c[0] = " << m_c[0] << ".");
    }
    
    costold = cost;
    cost = cost_eval(m_dynmag.data(),m_dynphs.data(),m_Ac.data(),m_m.data(),m_basemag.data(),m_basephs.data(),m_lam,n);
    if(m_verbose){
      RTHLOGGER_INFO(objectName() << "masked cost = " << cost << ".");
    }
	
  }  

  // copy final temp phase map to output
  for(i = 0;i < n;i++) mout[i].real(m_m[i]);

}

void thermohybrid::cleanup() 
{

  m_m.clear();
  m_dynmag.clear();
  m_dynphs.clear();
  m_basemag.clear();
  m_basephs.clear();
  m_Ac.clear();
  m_A.clear();
  m_c.clear();
  m_mask.clear();
  m_rebuildvectors = true;

}

int thermohybrid::order()
{
  return m_order;
}

void thermohybrid::setOrder(int order)
{
  m_order = order;
  m_rebuildvectors = true;
}

float thermohybrid::lam()
{
  return m_lam;
}

void thermohybrid::setLam(float lam)
{
  m_lam = lam;
}

float thermohybrid::maskthresh()
{
  return m_maskthresh;
}

void thermohybrid::setMaskthresh(float maskthresh)
{
  m_maskthresh = maskthresh;
}

float thermohybrid::stopthresh()
{
  return m_stopthresh;
}

void thermohybrid::setStopthresh(float stopthresh)
{
  m_stopthresh = stopthresh;
}

bool thermohybrid::verbose()
{
  return m_verbose;
}

void thermohybrid::setVerbose(bool verbose)
{
  m_verbose = verbose;
}

float cost_eval(float *dynmag,float *dynphs,float *Ac,float *m,float *basemag,
		float *basephs,float lam,int n){

  float cost = 0;

  for(int i = 0;i < n;i++){
    cost += 0.5*(dynmag[i]*dynmag[i]+basemag[i]*basemag[i]); /* constants */
    cost -= dynmag[i]*basemag[i]*cos(m[i] + Ac[i] + basephs[i] - dynphs[i]); /* 2*real()... */
    cost += lam*m[i]; /* sparsity */
  }
  
  return cost;
  
}

void m_update(float *dynmag,float *dynphs,float *Ac,float *m,float *basemag,
	      float *basephs,float lam,int n,bool *mask){

  int i;//,j;
  float grad,hess,t2,t3,t4,t5,magprod;
  for(i = 0;i < n;i++){ /* loop over spatial locs */
    if(mask[i]){

      grad = 0;hess = 0;
      //for(j = 0;j < nc;i++){ /* loop over rx coils */
      
      t2 = Ac[i] + m[i] + basephs[i] - dynphs[i];
      t3 = sin(t2);
      t4 = t2 - TWOPI*floor((t2+PI)/TWOPI);
      magprod = dynmag[i]*basemag[i];
      t5 = magprod*t3;
      
      grad += t5;
      if(t4*t4 > 0.04) hess += t5/t4; else hess += magprod;
      //if(hess < 0.0) hess *= -1.0;
            
      //}

      if(hess != 0) m[i] -= (grad+lam)/hess;
      m[i] = m[i] > 0 ? m[i] : 0;
      
    } // mask
    
  } // spatial locs
  
}

void c_update(float *dynmag,float *dynphs,
	      float *c,float *Ac,
	      float *m,float *basemag,float *basephs,int n){

  int i;
  float grad,hess,t2,t3,t4,t5,magprod;
  grad = 0;hess = 0;
  for(i = 0;i < n;i++){ /* loop over spatial locs */
      
    t2 = Ac[i] + m[i] + basephs[i] - dynphs[i];
    t3 = sin(t2);
    t4 = t2 - TWOPI*floor((t2+PI)/TWOPI);
    magprod = dynmag[i]*basemag[i];
    t5 = magprod*t3;
    
    grad += t5;
    if(t4*t4 > 0.04) hess += t5/t4; else hess += magprod;
    if(t4 != 0) hess += t5/t4;
      
  } // spatial locs
  
  //if(hess < 0.0) hess *= -1.0;

  if(hess != 0) c[0] -= grad/hess;  
  
}
