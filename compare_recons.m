% load in mat file, perform matlab processing, save results to compare with python version
load kdatalibimg_16

dim = 96;
te = 0.016; % s
tro = 0.0225;
ti = -tro/2:tro/dim:tro/2-tro/dim;

imglib = reshape(L,[dim dim 3]);
L = col(imglib(:,:,1));

ind = 15; 
imghot = squeeze(img(:,:,ind,1));

% fast cs recon
niters = 25;
order = 0;
lam = 10^-2;
[mhybr,Ahybr,chybr,fhybr] = thermo_hybrid_nounwrap(conj(imghot),order,lam,niters,conj(L));
mhybr = -1*mhybr;
fhybr = conj(fhybr);
%fhybr = abs(imghot).*exp(-1i*angle(fhybr));
chybr = -1*chybr;
imgc = fhybr.*reshape(exp(1i*Ahybr*chybr),[dim dim]);

% obtain a mask of 'significant' temperature rises
mask = mhybr < -0.1; % radians
% keep fhybr and chybr the same, dft one dim of k-space data to enable
% parallelization of that dim
cols = find(sum(mask,1));
% our goal is to minimize:
% sum_k |y_k - {A*f(mhybr)}_k|^2; since we are only updating spatial
% locs in the mask, we can subtract off signal from other locs
data1dft = imghot - ~mask.*imgc.*exp(1i*mhybr);
datamask_mat = data1dft;
% ft the frequency-encoded dimension to go back to k-space data
data1dft = ft(data1dft);
data1dft_mat = data1dft;

step = 0.0001;
%niters = 50;
mhybrsv = mhybr;
x = [-dim/2:dim/2-1]/dim;

for ii = cols
    % iteratively update the map inside the hot spot in each column
    data1col = data1dft(:,ii); % data for this column
    err = Inf;
    olderr = Inf;
    while err < 0.999*olderr || err == Inf;%for jj = 1:niters
        % build the system matrix for this column
        A = exp(-1i*2*pi*[-dim/2:dim/2-1]'*x(mask(:,ii))+1i*(col(ti(1,1:dim))/(te)+1)*real(mhybr(mask(:,ii),ii).'));
        Ai = bsxfun(@times,A,col(exp(-imag(mhybr(mask(:,ii),ii).'))).'.*col(imgc(mask(:,ii),ii)).');
        aux = data1col-sum(Ai,2);
        % calculate err
        olderr = err;
        err = norm(aux)
        % calculate gradient wrt temp map
        gr = real(2*1i*(Ai'*((col(ti(1,1:dim))/(te)+1).*aux)));
        gi = imag(2*1i*(Ai'*aux));
        % step in negative gradient direction
        mhybr(mask(:,ii),ii) = mhybr(mask(:,ii),ii) - step*(gr+1i*gi);
        m1 = mhybr;
        mhybr(mask(:,ii),ii) = real(mhybr(mask(:,ii),ii)).*(real(mhybr(mask(:,ii),ii)) < 0) + ...
            1i*imag(mhybr(mask(:,ii),ii)).*(imag(mhybr(mask(:,ii),ii)) > 0);
        m2 = mhybr;
    end
end

save fastcs2dftdata
